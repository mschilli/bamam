#!/bin/sh

##############################################################################
# BAMam bootstrap shell script                                               #
#                                                                            #
# Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>      #
#                                                                            #
# This file is part of BAMam.                                                #
#                                                                            #
# BAMam is free software: you can redistribute it and/or modify              #
# it under the terms of the GNU Affero General Public License as             #
# published by the Free Software Foundation, either version 3 of the         #
# License, or (at your option) any later version.                            #
#                                                                            #
# This program is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
# GNU Affero General Public License for more details.                        #
#                                                                            #
# You should have received a copy of the GNU Affero General Public License   #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
##############################################################################


#######################
# general information #
#######################

## @brief BAMam bootstrap shell script
#
#  This script bootstraps files required to build & install BAMam using GNU
#  Autotools.
#  After this initial bootstrap, `autoreconf` should be sufficient to update
#  all configuration files if needed.
#
#  @file bootstrap.sh
#  @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
#  @date 2019-01-07 - 2019-03-04

## @todo Trick doxygen into parsing this file as a Python script.


#############
# bootstrap #
#############

# Generate `aclocal.m4`, `config.h`,  missing GNU standard files, and
# `configure` script.
# Note: `autoreconf` does not (seem to) support `--add-missing`. That's why it
# is not used here.
aclocal \
&& autoheader \
&& automake --gnu --add-missing \
&& autoconf
