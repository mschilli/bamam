# BAMam: **BAM** file **a**nnotation with **m**utations


## About

This repository is used to develop BAMam, a tool to annotate BAM files with
the mutations occurring in each read.
BAMam is an attempt to speed-up a pipeline doing the same using `awk`.
Also, it is used as a 'toy example' on GNU-style C development.


## Copyright

Copyright © 2019 [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)


## License

BAMam is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version (GNU AGPL v3+).

See the [`LICENSE`](/LICENSE) file for a copy of the GNU AGPL v3 or check
<http://www.gnu.org/licenses/>.

BAMam bundles [a copy](/test/sharness/sharness.sh) of the [Sharness
projects][sharness]'s [`sharness.sh`][sharness-sh] script which is
used to test BAMam.
That script was originally released under the GNU General Public License
version 2, with the option to redistribute it and/or modify it under the terms
of any later version of the License (GNU GPL v2+).
The copy of that script bundled with BAMam is distributed under the terms of
the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version (GNU GPL
v3+).

See the [`test/sharness/LICENSE`](/test/sharness/LICENSE) file for a copy of
the GNU GPL v3 or check <http://www.gnu.org/licenses/>.

While work released under the GPL v3 cannot be re-licensed under the AGPL v3+,
the two licenses are compatible in the sense of linking AGPL v3 against GPL v3
work.
The copy of the `sharness.sh` script distributed with BAMam was left unmodified
(except for the GPL version increase from v2+ to v3+) and is only sourced as-is
at runtime by the BAMam test shell scripts. This is the shell script equivalent
of linking against a library and therefore in-line with the terms of both
licenses.


## Requirements

BAMam is developed and supposed to run on a 64bit GNU/Linux system.
However, it should be portable to other systems given its very basic nature.


## Build/installation

BAMam is developed using the [GNU build system][autotools].

So a simple

```sh
# Bootstrapping is required only when starting from a git checkout.
# Building from a distribution tarball, it is not necessary to run that script.
#./bootstrap.sh
./configure
make
make install
```

should be sufficient.


## Development

BAMam development is versioned via [Git][git].
Code should be written in line with the [GNU C standard][standard].


[sharness]: https://github.com/chriscool/sharness
[sharness-sh]: https://github.com/chriscool/sharness/blob/b28afa1fda46996e46b9140c8c099eda73db6573/sharness.sh
[autotools]: https://www.gnu.org/software/automake/manual/html_node/GNU-Build-System.html
[git]: https://git-scm.com
[standard]: https://www.gnu.org/prep/standards/html_node/Writing-C.html
