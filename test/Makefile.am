##############################################################################
# BAMam test Makefile.am file                                                #
#                                                                            #
# Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>      #
#                                                                            #
# This file is part of BAMam.                                                #
#                                                                            #
# BAMam is free software: you can redistribute it and/or modify              #
# it under the terms of the GNU Affero General Public License as             #
# published by the Free Software Foundation, either version 3 of the         #
# License, or (at your option) any later version.                            #
#                                                                            #
# This program is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
# GNU Affero General Public License for more details.                        #
#                                                                            #
# You should have received a copy of the GNU Affero General Public License   #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
##############################################################################


#######################
# general information #
#######################

## @brief BAMam test `Makefile.am` file
#
#  This file describes how to genereate the test `Makefile` to test BAMam using
#  GNU Automake.
#
#  @file Makefile.am
#  @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
#  @date 2019-01-08 - 2019-04-12

## @todo Trick doxygen into parsing this file as a Python script.


#########
# setup #
#########

# Ensure tests can find all required files.
AM_TESTS_ENVIRONMENT= \
## Pass down the path of the Sharness shell test library itself.
## Note: As this file is added to `EXTRA_DIST` (see finalization section on the
## bottom of this file), it can safely be assumed to be found in the source
## directory. However, there is nothing that would copy it to the build
## directory if BAMam is build outside of the source directory. Thus, the
## source directory path is used here.
  sharness=$(abs_srcdir)/sharness/sharness.sh; \
  export sharness; \
## Pass down the path of the test data directory.
## Note: As the test data files are added to `EXTRA_DIST` (see finalization
## section on the bottom of this file), they can safely be assumed to be found
## in the source directory. However, there is nothing that would copy it to the
## build directory if BAMam is build outside of the source directory. Thus, the
## source directory path is used here.
  test_data_dir=$(abs_srcdir); \
  export test_data_dir; \
## Pass down the build directory to the Sharness shell test library to ensure
## `bamam` can be found on `PATH` during tests.
  SHARNESS_BUILD_DIRECTORY=$(abs_top_builddir)/src; \
  export SHARNESS_BUILD_DIRECTORY;


#########
# tests #
#########

# Define the test shell scripts to run.
TESTS = bamam.test md.test nucleotides.test cigar.test sam.test args.test \
        messages.test


########
# lint #
########

# Lint shell scripts and fail on any error.
.PHONY: lint-local
lint-local:
	shellcheck --shell=sh $(TESTS)


###########
# cleanup #
###########

# Cleanup test output direcrories.
# Note: Setting `CLEANFILES` does not work for directories, so a `clean-local`
# rule is used instead.
.PHONY: clean-local
clean-local:
	rm -rf test-results


################
# finalization #
################

# Include test files in distribution tarball.
# The bundled Sharness shell test library also needs to be included to
# enable running the tests from the distribution files. As per the terms of
# the GPL this script was licensed under, distributing the script requires
# distributing a copy of the license with it.
EXTRA_DIST = $(TESTS) \
             good.sam good.out \
             too_few_fields_before_sam_flag.sam \
             too_few_fields_sam_flag_last.sam \
             invalid_sam_flag.sam \
             too_few_fields_before_cigar.sam \
             too_few_fields_cigar_last.sam \
             too_few_fields_before_seq.sam \
             too_few_fields_seq_last.sam \
             too_few_fields_no_tag.sam \
             missing_md.sam \
             invalid_cigar_missing_count_begin.sam \
             invalid_cigar_missing_count.sam \
             invalid_cigar_missing_char.sam \
             invalid_cigar_invalid_char.sam \
             invalid_md_missing_match_count_begin.sam \
             invalid_md_missing_match_count.sam \
             invalid_md_missing_deletion_nucleotide_end.sam \
             invalid_md_missing_deletion_nucleotide.sam \
             invalid_md_invalid_deletion_nucleotide.sam \
             invalid_md_invalid_mismatch_nucleotide.sam \
             incompatible_sequence_and_quality.sam \
             incompatible_cigar_and_sequence.sam \
             incompatible_cigar_and_md.sam \
             invalid_nucleotide.sam \
             sharness/sharness.sh sharness/LICENSE
