#!/bin/sh

##############################################################################
# BAMam CIGAR parsing test shell script                                      #
#                                                                            #
# Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>      #
#                                                                            #
# This file is part of BAMam.                                                #
#                                                                            #
# BAMam is free software: you can redistribute it and/or modify              #
# it under the terms of the GNU Affero General Public License as             #
# published by the Free Software Foundation, either version 3 of the         #
# License, or (at your option) any later version.                            #
#                                                                            #
# This program is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
# GNU Affero General Public License for more details.                        #
#                                                                            #
# You should have received a copy of the GNU Affero General Public License   #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
##############################################################################


#######################
# general information #
#######################

## @brief BAMam CIGAR parsing test shell script.
#
#  This script tests the CIGAR parsing of the BAMam binary.
#  The tests are implemented using the Sharness shell test library generating
#  TAP output.
#
#  @file cigar.test
#  @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
#  @date 2019-01-29 - 2019-04-11

## @todo Trick doxygen into parsing this file as a Python script.


##################
# initialization #
##################

# Describe the test.
# 'Unused' variable is actually used by sourced code:
# shellcheck disable=SC2034
test_description="\
  Run \`bamam\` and ensure it does not fail and outputs the correct output."

# Source the Sharness shell test library.
# Do not lint Sharness shell test library itself:
# shellcheck source=/dev/null
. "${sharness:-sharness/sharness.sh}"


#########
# tests #
#########

# Run `bamam < invalid_cigar_missing_count_begin.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count_begin.sam\` fails." \
"
  test_must_fail bamam \
    < '${test_data_dir:-..}'/invalid_cigar_missing_count_begin.sam
"

# Run `bamam < invalid_cigar_missing_count_begin.sam` and ensure it fails with
# the expected error code.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count_begin.sam\` fails with the \
expected error code." \
"
  test_expect_code 5 \
    bamam < '${test_data_dir:-..}'/invalid_cigar_missing_count_begin.sam
"

# Run `bamam < invalid_cigar_missing_count_begin.sam`, capture STDERR in a
# file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count_begin.sam\` produces output \
to STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_cigar_missing_count_begin.sam 2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_cigar_missing_count.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_cigar_missing_count.sam
"

# Run `bamam < invalid_cigar_missing_count.sam` and ensure it fails with the
# expected error code.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count.sam\` fails with the \
expected error code." \
"
  test_expect_code 5 \
    bamam < '${test_data_dir:-..}'/invalid_cigar_missing_count.sam
"

# Run `bamam < invalid_cigar_missing_count.sam`, capture STDERR in a file, and
# ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_count.sam\` produces output to \
STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_cigar_missing_count.sam 2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_cigar_missing_char.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_char.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_cigar_missing_char.sam
"

# Run `bamam < invalid_cigar_missing_char.sam` and ensure it fails with the
# expected error code.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_char.sam\` fails with the expected \
error code." \
"
  test_expect_code 5 \
    bamam < '${test_data_dir:-..}'/invalid_cigar_missing_char.sam
"

# Run `bamam < invalid_cigar_missing_char.sam`, capture STDERR in a file, and
# ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_cigar_missing_char.sam\` produces output to \
STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_cigar_missing_char.sam 2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_cigar_invalid_char.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_cigar_invalid_char.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_cigar_invalid_char.sam
"

# Run `bamam < invalid_cigar_invalid_char.sam` and ensure it fails with the
# expected error code.
test_expect_success \
  "Running \`bamam < invalid_cigar_invalid_char.sam\` fails with the expected \
error code." \
"
  test_expect_code 5 \
    bamam < '${test_data_dir:-..}'/invalid_cigar_invalid_char.sam
"

# Run `bamam < invalid_cigar_invalid_char.sam`, capture STDERR in a file, and
# ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_cigar_invalid_char.sam\` produces output to \
STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_cigar_invalid_char.sam 2>stderr
  [ -s stderr ]
"


################
# finalization #
################

# Cleanup test files and summarize test results.
test_done
