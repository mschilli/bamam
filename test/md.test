#!/bin/sh

##############################################################################
# BAMam MD tag parsing test shell script                                     #
#                                                                            #
# Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>      #
#                                                                            #
# This file is part of BAMam.                                                #
#                                                                            #
# BAMam is free software: you can redistribute it and/or modify              #
# it under the terms of the GNU Affero General Public License as             #
# published by the Free Software Foundation, either version 3 of the         #
# License, or (at your option) any later version.                            #
#                                                                            #
# This program is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
# GNU Affero General Public License for more details.                        #
#                                                                            #
# You should have received a copy of the GNU Affero General Public License   #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
##############################################################################


#######################
# general information #
#######################

## @brief BAMam MD tag parsing test shell script.
#
#  This script tests the MD tag parsing of the BAMam binary.
#  The tests are implemented using the Sharness shell test library generating
#  TAP output.
#
#  @file md.test
#  @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
#  @date 2019-03-04 - 2019-04-10

## @todo Trick doxygen into parsing this file as a Python script.


##################
# initialization #
##################

# Describe the test.
# 'Unused' variable is actually used by sourced code:
# shellcheck disable=SC2034
test_description="\
  Run \`bamam\` with input containing invalid MD tags and ensure it fails \
with the expected error code and an error message."

# Source the Sharness shell test library.
# Do not lint Sharness shell test library itself:
# shellcheck source=/dev/null
. "${sharness:-sharness/sharness.sh}"


#########
# tests #
#########

# Run `bamam < invalid_md_missing_match_count_begin.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count_begin.sam\` fails." \
"
  test_must_fail bamam \
    < '${test_data_dir:-..}'/invalid_md_missing_match_count_begin.sam
"

# Run `bamam < invalid_md_missing_match_count_begin.sam` and ensure it fails
# with the expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count_begin.sam\` fails with \
the expected error code." \
"
  test_expect_code 6 \
    bamam < '${test_data_dir:-..}'/invalid_md_missing_match_count_begin.sam
"

# Run `bamam < invalid_md_missing_match_count_begin.sam`, capture STDERR in a
# file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count_begin.sam\` produces \
output to STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_md_missing_match_count_begin.sam \
    2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_md_missing_match_count.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_md_missing_match_count.sam
"

# Run `bamam < invalid_md_missing_match_count.sam` and ensure it fails with the
# expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count.sam\` fails with the \
expected error code." \
"
  test_expect_code 6 \
    bamam < '${test_data_dir:-..}'/invalid_md_missing_match_count.sam
"

# Run `bamam < invalid_md_missing_match_count.sam`, capture STDERR in a file,
# and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_missing_match_count.sam\` produces output to \
STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_md_missing_match_count.sam 2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_md_missing_deletion_nucleotide_end.sam` and ensure it
# fails.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide_end.sam\` fails." \
"
  test_must_fail \
    bamam \
    < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide_end.sam
"

# Run `bamam < invalid_md_missing_deletion_nucleotide_end.sam` and ensure it
# fails with the expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide_end.sam\` fails \
with the expected error code." \
"
  test_expect_code 6 \
    bamam \
      < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide_end.sam
"

# Run `bamam < invalid_md_missing_deletion_nucleotide_end.sam`, capture STDERR
# in a file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide_end.sam\` \
produces output to STDERR." \
"
  bamam \
    < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide_end.sam \
    2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_md_missing_deletion_nucleotide.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide.sam
"

# Run `bamam < invalid_md_missing_deletion_nucleotide.sam` and ensure it fails
# with the expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide.sam\` fails with \
the expected error code." \
"
  test_expect_code 6 \
    bamam < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide.sam
"

# Run `bamam < invalid_md_missing_deletion_nucleotide.sam`, capture STDERR in a
# file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_missing_deletion_nucleotide.sam\` produces \
output to STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_md_missing_deletion_nucleotide.sam \
    2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_md_deletion_nucleotide.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_deletion_nucleotide.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_md_invalid_deletion_nucleotide.sam
"

# Run `bamam < invalid_md_invalid_deletion_nucleotide.sam` and ensure it fails
# with the expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_deletion_nucleotide.sam\` fails with
the expected error code." \
"
  test_expect_code 6 \
    bamam < '${test_data_dir:-..}'/invalid_md_invalid_deletion_nucleotide.sam
"

# Run `bamam < invalid_md_invalid_deletion_nucleotide.sam`, capture STDERR in a
# file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_deletion_nucleotide.sam\` produces \
output to STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_md_invalid_deletion_nucleotide.sam \
    2>stderr
  [ -s stderr ]
"

# Run `bamam < invalid_md_mismatch_nucleotide.sam` and ensure it fails.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_mismatch_nucleotide.sam\` fails." \
"
  test_must_fail \
    bamam < '${test_data_dir:-..}'/invalid_md_invalid_mismatch_nucleotide.sam
"

# Run `bamam < invalid_md_invalid_mismatch_nucleotide.sam` and ensure it fails
# with the expected error code.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_mismatch_nucleotide.sam\` fails with
the expected error code." \
"
  test_expect_code 6 \
    bamam < '${test_data_dir:-..}'/invalid_md_invalid_mismatch_nucleotide.sam
"

# Run `bamam < invalid_md_invalid_mismatch_nucleotide.sam`, capture STDERR in a
# file, and ensure this file is not empty.
test_expect_success \
  "Running \`bamam < invalid_md_invalid_mismatch_nucleotide.sam\` produces \
output to STDERR." \
"
  bamam < '${test_data_dir:-..}'/invalid_md_invalid_mismatch_nucleotide.sam \
    2>stderr
  [ -s stderr ]
"


################
# finalization #
################

# Cleanup test files and summarize test results.
test_done
