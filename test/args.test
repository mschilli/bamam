#!/bin/sh

##############################################################################
# BAMam argument parsing test shell script                                   #
#                                                                            #
# Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>      #
#                                                                            #
# This file is part of BAMam.                                                #
#                                                                            #
# BAMam is free software: you can redistribute it and/or modify              #
# it under the terms of the GNU Affero General Public License as             #
# published by the Free Software Foundation, either version 3 of the         #
# License, or (at your option) any later version.                            #
#                                                                            #
# This program is distributed in the hope that it will be useful,            #
# but WITHOUT ANY WARRANTY; without even the implied warranty of             #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
# GNU Affero General Public License for more details.                        #
#                                                                            #
# You should have received a copy of the GNU Affero General Public License   #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
##############################################################################


#######################
# general information #
#######################

## @brief BAMam argument parsing test shell script                                   #
#
#  This script tests the argument parsing of the BAMam binary.
#  The tests are implemented using the Sharness shell test library generating
#  TAP output.
#
#  @file args.test
#  @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
#  @date 2019-01-11 - 2019-01-28

## @todo Trick doxygen into parsing this file as a Python script.


##################
# initialization #
##################

# Describe the test.
# 'Unused' variable is actually used by sourced code:
# shellcheck disable=SC2034
test_description="Test BAMam's command line argument parsing."

# Source the Sharness shell test library.
# Do not lint Sharness shell test library itself:
# shellcheck source=/dev/null
. "${sharness:-sharness/sharness.sh}"


#########
# tests #
#########

# Ensure `bamam -h` doesn't fail.
test_expect_success "Running \`bamam -h\` doesn't fail." \
'
  bamam -h
'

# Ensure `bamam -h` produces output to STDOUT.
test_expect_success "Running \`bamam -h\` produces output to STDOUT." \
'
  bamam -h >stdout
  [ -s stdout ]
'

# Ensure `bamam -h` doesn't produce any output to STDERR.
test_expect_success \
  "Running \`bamam -h\` doesn't produce any output to STDERR." \
'
  bamam -h 2>stderr
  test_must_be_empty stderr
'

# Ensure `bamam --help` doesn't fail.
test_expect_success "Running \`bamam --help\` doesn't fail." \
'
  bamam --help
'

# Ensure `bamam --help` produces output to STDOUT.
test_expect_success "Running \`bamam --help\` produces output to STDOUT." \
'
  bamam --help >stdout
  [ -s stdout ]
'

# Ensure `bamam --help` doesn't produce any output to STDERR.
test_expect_success \
  "Running \`bamam --help\` doesn't produce any output to STDERR." \
'
  bamam --help 2>stderr
  test_must_be_empty stderr
'

# Ensure `bamam -V` doesn't fail.
test_expect_success "Running \`bamam -V\` doesn't fail." \
'
  bamam -V
'

# Ensure `bamam -V` produces output to STDOUT.
test_expect_success "Running \`bamam -V\` produces output to STDOUT." \
'
  bamam -V >stdout
  [ -s stdout ]
'

# Ensure `bamam -V` doesn't produce any output to STDERR.
test_expect_success \
  "Running \`bamam -V\` doesn't produce any output to STDERR." \
'
  bamam -V 2>stderr
  test_must_be_empty stderr
'

# Ensure `bamam --version` doesn't fail.
test_expect_success "Running \`bamam --version\` doesn't fail." \
'
  bamam --version
'

# Ensure `bamam --version` produces output to STDOUT.
test_expect_success "Running \`bamam --version\` produces output to STDOUT." \
'
  bamam --version >stdout
  [ -s stdout ]
'

# Ensure `bamam --version` doesn't produce any output to STDERR.
test_expect_success \
  "Running \`bamam --version\` doesn't produce any output to STDERR." \
'
  bamam --version 2>stderr
  test_must_be_empty stderr
'

# Ensure `bamam -he` fails.
test_expect_success "Running \`bamam -he\` fails." \
'
  test_must_fail bamam -he
'

# Ensure `bamam -he` returns the correct error code.
test_expect_success "Running \`bamam -he\` returns the correct error code." \
'
  test_expect_code 2 bamam -he
'

# Ensure `bamam -he` doesn't produce any output to STDOUT.
test_expect_success \
  "Running \`bamam -he\` doesn't produce any output to STDOUT." \
'
  bamam -he >stdout
  test_must_be_empty stdout
'

# Ensure `bamam -he` produces output to STDERR.
test_expect_success "Running \`bamam -he\` produces output to STDERR." \
'
  bamam h -he 2>stderr
  [ -s stderr ]
'

# Ensure `bamam --he` fails.
test_expect_success "Running \`bamam --he\` fails." \
'
  test_must_fail bamam --he
'

# Ensure `bamam --he` returns the correct error code.
test_expect_success "Running \`bamam --he\` returns the correct error code." \
'
  test_expect_code 2 bamam --he
'

# Ensure `bamam --he` doesn't produce any output to STDOUT.
test_expect_success "Running \`bamam --he\` doesn't produce any output to \
STDOUT." \
'
  bamam --he >stdout
  test_must_be_empty stdout
'

# Ensure `bamam --he` produces output to STDERR.
test_expect_success "Running \`bamam --he\` produces output to STDERR." \
'
  bamam h --he 2>stderr
  [ -s stderr ]
'

# Ensure `bamam -h -V` fails.
test_expect_success "Running \`bamam -h -V\` fails." '
  test_must_fail bamam -h -V
'

# Ensure `bamam -h -V` returns the correct error code.
test_expect_success "Running \`bamam -h -V\` returns the correct error code." \
'
  test_expect_code 1 bamam -h -V
'

# Ensure `bamam -h -V` doesn't produce any output to STDOUT.
test_expect_success \
  "Running \`bamam -h -V\` doesn't produce any output to STDOUT." \
'
  bamam -h -V >stdout
  test_must_be_empty stdout
'

# Ensure `bamam -h -V` produces output to STDERR.
test_expect_success "Running \`bamam -h -V\` produces output to STDERR." \
'
  bamam -h -V 2>stderr
  [ -s stderr ]
'


################
# finalization #
################

# Cleanup test files and summarize test results.
test_done
