/*****************************************************************************
 * BAMam CIGAR parsing library header file                                   *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam CIGAR parsing library header file.
 *
 * This is the header file for BAMam's CIGAR parsing library.
 *
 * @file cigar.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-29 - 2019-04-09
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_CIGAR_H
#define BAMAM_CIGAR_H


/************
 * includes *
 ************/

/*
 * Include standard definitions library to get the following type:
 * - `size_t` for unsigned integers large enough to hold any array index.
 */
#include <stddef.h>


/*************
 * functions *
 *************/

/**
 * @brief Get next CIGAR character.
 *
 * This function returns the location of the next CIGAR character within the
 * CIGAR string (suffix) based on the beginning of the next entry within the
 * CIGAR string (suffix), the (remaining) length of the CIGAR string (suffix)
 * from that position on, and the (remaining) count of the current CIGAR entry.
 * If the processed part of the CIGAR string (suffix) is valid, not only is the
 * location of the next character of the CIGAR string (suffix) returned, but
 * also are the corresponding location of the beginning of the next CIGAR entry,
 * the remaining count of the current CIGAR entry, and the remaining CIGAR
 * string suffix length written to the locations pointed at by @p cursor,
 * @p count and @p len, respectively, as well as the no-error code to the
 * location pointed at by @p status to indicate success. If this is not the
 * case on the other hand, an error message is printed to STDERR, an error code
 * is written to the location pointed to by @p status to indicate the
 * invalidity of the CIGAR entry, null is written to the location pointed to by
 * @pa cursor, zero is written to the locations pointed to by @p count and
 * @p len, and the null pointer is returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location to read/write the location of the beginning (i.e.
 *               first digit) of the next CIGAR entry to parse from/to.
 * @param len Location to read/write length of the CIGAR string (suffix)
 *            pointed to by the pointer pointed to by @p cursor from/to.
 * @param count Location to read/write the count of the current CIGAR entry
 *              from/to.
 * @param status Location to write the (non-)error code to.
 * @return Location of the next CIGAR character within the CIGAR string
 *         (suffix), if the processed part of the CIGAR string (suffix) is
 *         valid, the null pointer otherwise.
 */
const char *
get_next_cigar_char(const char const **cursor, size_t *len, size_t *count,
                    int *status);


/**
 * @brief Check if character is deletion CIGAR character.
 *
 * This function checks for a single character, if it is a deletion CIGAR
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is a deletion CIGAR character, zero otherwise.
 */
unsigned char
is_deletion_char(const char character);


/**
 * @brief Check if character is skipping CIGAR character.
 *
 * This function checks for a single character, if it is a skipping CIGAR
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is a skipping CIGAR character, zero otherwise.
 */
unsigned char
is_skipping_char(const char character);


/**
 * @brief Check if character is soft-clipping CIGAR character.
 *
 * This function checks for a single character, if it is a soft-clipping CIGAR
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is a soft-clipping CIGAR character, zero
 *         otherwise.
 */
unsigned char
is_soft_clipping_char(const char character);


/**
 * @brief Check if character is insertion CIGAR character.
 *
 * This function checks for a single character, if it is an insertion CIGAR
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is an insertion CIGAR character, zero otherwise.
 */
unsigned char
is_insertion_char(const char character);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_CIGAR_H */
