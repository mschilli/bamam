/*****************************************************************************
 * BAMam message printing library header file                                *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam message printing library header file.
 *
 * This is the header file for BAMam's message printing library.
 * The functionality provided here could be achieved using the getopt library
 * but given that BAMam is not supposed to take any command line arguments but
 * the standard help and version flags, even a dependency this small and common
 * seems unnecessary.
 *
 * @file messages.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-11 - 2019-04-12
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_MESSAGES_H
#define BAMAM_MESSAGES_H


/************
 * includes *
 ************/

/*
 * Include standard definitions library to get the following type:
 * - `size_t` for unsigned integers large enough to hold any array index.
 */
#include <stddef.h>


/*************
 * constants *
 *************/

/**
 * @brief Usage error message.
 *
 * This string constant will be printed upon any invalid BAMam call.
 */
static const char *const USAGE_ERROR_MESSAGE =
  "ERROR: Invalid usage!\n";


/**
 * @brief Too-many-arguments error message prefix.
 *
 * This string constant will be appended by the given number of arguments, the
 * too-many-arguments error message infix, the maximum valid number of
 * arguments, and the too-many-arguments error message suffix to obtain the
 * full too-many-arguments error message.
 */
static const char *const TOO_MANY_ARGUMENTS_ERROR_MESSAGE_PRE =
  "  Too many arguments: ";


/**
 * @brief Too-many-arguments error message infix.
 *
 * This string constant will be appended to the too-many-arguments error
 * message prefix and the given number of arguments and appended by the maximum
 * valid number or arguments and the too-many-arguments error message suffix to
 * obtain the full too-many-arguments error message.
 */
static const char *const TOO_MANY_ARGUMENTS_ERROR_MESSAGE_MID =
  " given, up to ";


/**
 * @brief Too-many-arguments error message suffix.
 *
 * This string constant will be appended to the too-many-arguments error
 * message prefix, the given number of arguments, the too-many-arguments error
 * message infix, and the maximum valid number or arguments to obtain the full
 * too-many-arguments error message.
 */
static const char *const TOO_MANY_ARGUMENTS_ERROR_MESSAGE_POST =
  " supported.\n"
  "\n";


/**
 * @brief Invalid-flag error message prefix.
 *
 * This string constant will be appended by the invalid flag and the invalid
 * flag error message suffix to obtain the full invalid-flag error message.
 */
static const char *const INVALID_FLAG_ERROR_MESSAGE_PRE =
  "  Invalid flag: '";


/**
 * @brief Invalid-flag error message suffix.
 *
 * This string constant will be appended to the invalid-flag error message
 * prefix and the invalid flag to obtain the full invalid-flag error message.
 */
static const char *const INVALID_FLAG_ERROR_MESSAGE_POST =
  "'.\n"
  "\n";


/**
 * @brief Usage message prefix.
 *
 * This string constant will be appended by the command used to call BAMam and
 * the usage message suffix to obtain the full usage message.
 */
static const char *const USAGE_MESSAGE_PRE =
  "Usage:\n"
  "  ";


/**
 * @brief Usage message infix.
 *
 * This string constant will be appended to the usage message prefix and the
 * command used to call BAMam to obtain the full usage message.
 */
static const char *const USAGE_MESSAGE_POST =
  " [flag]\n"
  "\n";


/**
 * @brief Help hint message prefix.
 *
 * This string constant will be appended by the command used to call BAMam,
 * and the help hint message suffix to obtain the full help hint message.
 */
static const char *const HELP_HINT_MESSAGE_PRE =
  "See '";


/**
 * @brief Help hint message suffix.
 *
 * This string constant will be appended to the help hint message prefix and
 * the command used to call BAMam to obtain the full help hint message.
 */
static const char *const HELP_HINT_MESSAGE_POST =
  " -h' for more details.\n";


/**
 * @brief Help message prefix.
 *
 * This string constant will be appended by the usage message and the help
 * message suffix to obtain the full help message.
 */
static const char *const HELP_MESSAGE_PRE =
  "BAMam - BAM file annotation with mutations\n"
  "\n";


/**
 * @brief Help message suffix.
 *
 * This string constant will be appended to help message prefix and the usage
 * message to obtain the full help message.
 */
static const char *const HELP_MESSAGE_POST =
  "Flags:\n"
  "  -h, --help     Print this help message and exit.\n"
  "  -V, --version  Print the version number and exit.\n";


/**
 * @brief Invalid-line error message.
 *
 * This string constant will be printed upon any invalid input line.
 */
static const char *const INVALID_LINE_ERROR_MESSAGE=
  "ERROR: Invalid SAM input line!\n";


/**
 * @brief Too-few-fields error message.
 *
 * This string constant will be printed upon any input line with too few
 * fields.
 */
static const char *const TOO_FEW_FIELDS_ERROR_MESSAGE=
  "  Too few SAM fields:";


/**
 * @brief No-SAM-flag-field error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full no-SAM-flag-field error message.
 */
static const char *const NO_SAM_FLAG_FIELD_ERROR_MESSAGE_PRE=
  " No SAM flag field found in input line:\n";


/**
 * @brief SAM-flag-field-last error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full SAM-flag-field-last error message.
 */
static const char *const SAM_FLAG_FIELD_LAST_ERROR_MESSAGE_PRE=
  " SAM flag field must not be the last field:\n";


/**
 * @brief invalid-SAM-flag error message prefix.
 *
 * This string constant will be appended by the offending SAM flag field, the
 * invalid-SAM-flag error message infix, and offending line to obtain the full
 * invalid-SAM-flag error message.
 */
static const char *const INVALID_SAM_FLAG_ERROR_MESSAGE_PRE=
  " Invalid SAM flag: ";


/**
 * @brief invalid-SAM-flag error message infix.
 *
 * This string constant will be appended to the invalid-SAM-flag error message
 * prefix and the offending SAM flag field, and appended by the offending line
 * to obtain the full invalid-SAM-flag error message.
 */
static const char *const INVALID_SAM_FLAG_ERROR_MESSAGE_MID=
  "\n";


/**
 * @brief No-CIGAR-field error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full no-CIGAR-field error message.
 */
static const char *const NO_CIGAR_FIELD_ERROR_MESSAGE_PRE=
  " No CIGAR field found in input line:\n";


/**
 * @brief CIGAR-field-last error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full CIGAR-field-last error message.
 */
static const char *const CIGAR_FIELD_LAST_ERROR_MESSAGE_PRE=
  " CIGAR field must not be the last field:\n";


/**
 * @brief No-sequence-field error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full no-sequence-field error message.
 */
static const char *const NO_SEQ_FIELD_ERROR_MESSAGE_PRE=
  " No sequence field found in input line:\n";


/**
 * @brief Sequence-field-last error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full sequence-field-last error message.
 */
static const char *const SEQ_FIELD_LAST_ERROR_MESSAGE_PRE=
  " Sequence field must not be the last field:\n";


/**
 * @brief No-tag error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full no-tag error message.
 */
static const char *const NO_TAG_ERROR_MESSAGE_PRE=
  " No tag found in input line:\n";


/**
 * @brief No-MD-tag error message prefix.
 *
 * This string constant will be appended by the offending line to obtain the
 * full no-MD-tag error message.
 */
static const char *const NO_MD_TAG_ERROR_MESSAGE_PRE=
  " No MD tag found in input line:\n";


/**
 * @brief Invalid-CIGAR error message.
 *
 * This string constant will be printed upon any invalid CIGAR string.
 */
static const char *const INVALID_CIGAR_ERROR_MESSAGE=
  "ERROR: Invalid CIGAR string!\n";


/**
 * @brief Missing-CIGAR-count error message prefix.
 *
 * This string constant will be appended by the offending CIGAR string suffix
 * and the missing-CIGAR-count error message suffix to obtain the full
 * missing-CIGAR-count error message.
 */
static const char *const MISSING_CIGAR_COUNT_ERROR_MESSAGE_PRE=
  "  Character must be prefixed by count: ";


/**
 * @brief Missing-CIGAR-count error message suffix.
 *
 * This string constant will be appended to the missing-CIGAR-count error
 * message prefix and the offending CIGAR string suffix to obtain the full
 * missing-CIGAR-count error message.
 */
static const char *const MISSING_CIGAR_COUNT_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Missing-CIGAR-character error message prefix.
 *
 * This string constant will be appended by the offending CIGAR count and the
 * missing-CIGAR-character error message suffix to obtain the full
 * missing-CIGAR-character error message.
 */
static const char *const MISSING_CIGAR_CHAR_ERROR_MESSAGE_PRE=
  "  Count must be followed by character: ";


/**
 * @brief Missing-CIGAR-character error message suffix.
 *
 * This string constant will be appended to the missing-CIGAR-character error
 * message prefix and the offending CIGAR count to obtain the full
 * missing-CIGAR-character error message.
 */
static const char *const MISSING_CIGAR_CHAR_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Invalid-CIGAR-character error message prefix.
 *
 * This string constant will be appended by the offending CIGAR string suffix
 * and the invalid-CIGAR-character error message suffix to obtain the full
 * invalid-CIGAR-character error message.
 */
static const char *const INVALID_CIGAR_CHAR_ERROR_MESSAGE_PRE=
  "  Invalid CIGAR character: ";


/**
 * @brief Invalid-CIGAR-character error message suffix.
 *
 * This string constant will be appended to the invalid-CIGAR-character error
 * message prefix and the offending CIGAR string suffix to obtain the full
 * invalid-CIGAR-character error message.
 */
static const char *const INVALID_CIGAR_CHAR_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Invalid-MD-tag error message.
 *
 * This string constant will be printed upon any invalid MD tag.
 */
static const char *const INVALID_MD_TAG_ERROR_MESSAGE=
  "ERROR: Invalid MD tag!\n";


/**
 * @brief Missing-MD-match-count error message prefix.
 *
 * This string constant will be appended by the offending MD tag suffix and the
 * missing-MD-match-count error message suffix to obtain the full
 * missing-MD-match-count error message.
 */
static const char *const MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_PRE=
  "  Mismatch/deletion must be prefixed by match count: ";


/**
 * @brief Missing-MD-match-count error message suffix.
 *
 * This string constant will be appended to the missing-MD-match-count error
 * message prefix and the offending MD tag suffix to obtain the full
 * missing-MD-match-count error message.
 */
static const char *const MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Invalid-MD-mismatch-nucleotide error message prefix.
 *
 * This string constant will be appended by the offending MD tag suffix and the
 * invalid-MD-mismatch-nucleotide error message suffix to obtain the full
 * invalid-MD-mismatch-nucleotide error message.
 */
static const char *const INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_PRE=
  "  Invalid mismatch nucleotide: ";


/**
 * @brief Invalid-MD-mismatch-nucleotide error message suffix.
 *
 * This string constant will be appended to the invalid-MD-mismatch-nucleotide
 * error message prefix and the offending MD tag suffix to obtain the full
 * invalid-MD-mismatch-nucleotide error message.
 */
static const char *const INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Missing-MD-deletion-nucleotide error message prefix.
 *
 * This string constant will be appended by the offending MD tag suffix and the
 * missing-MD-deletion-nucleotide error message suffix to obtain the full
 * missing-MD-deletion-nucleotide error message.
 */
static const char *const MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE=
  "  Missing deletion nucleotide: ";


/**
 * @brief Missing-MD-mismatch-nucleotide error message suffix.
 *
 * This string constant will be appended to the missing-MD-deletion-nucleotide
 * error message prefix and the offending MD tag suffix to obtain the full
 * missing-MD-deletion-nucleotide error message.
 */
static const char *const MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Invalid-MD-deletion-nucleotide error message prefix.
 *
 * This string constant will be appended by the offending MD tag suffix and the
 * invalid-MD-deletion-nucleotide error message suffix to obtain the full
 * invalid-MD-deletion-nucleotide error message.
 */
static const char *const INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE=
  "  Invalid deletion nucleotide: ";


/**
 * @brief Invalid-MD-deletion-nucleotide error message suffix.
 *
 * This string constant will be appended to the invalid-MD-deletion-nucleotide
 * error message prefix and the offending MD tag suffix to obtain the full
 * invalid-MD-deletion-nucleotide error message.
 */
static const char *const INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST=
  "\n";


/**
 * @brief Incompatible-CIGAR-and-sequence error message.
 *
 * This string constant contains the to the incompatible-CIGAR-and-sequence error
 * message.
 */
static const char *const INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR_MESSAGE=
  "ERROR: Incompatible CIGAR string and sequence combination!\n";


/**
 * @brief Incompatible-sequence-and-quality error message prefix.
 *
 * This string constant will be appended by the offending sequence, the first
 * incompatible-sequence-and-quality error message infix, the offending
 * sequence's length, the second incompatible-sequence-and-quality error
 * message infix, the offending quality string, the third
 * incompatible-sequence-and-quality error message infix, the offending quality
 * string's length, the fourth incompatible-sequence-and-quality error message
 * infix, the offending input line, and the incompatible-sequence-and-quality
 * error message suffix, to obtain the full incompatible-sequence-and-quality
 * error message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_PRE=
  "ERROR: Incompatible sequence and quality string combination!\n"
  "  sequence: ";


/**
 * @brief First incompatible-sequence-and-quality error message infix.
 *
 * This string constant will be appended to the
 * incompatible-sequence-and-quality error message prefix, and the offending
 * sequence, and appended by the offending sequence's length, the second
 * incompatible-sequence-and-quality error message infix, the offending quality
 * string, the third incompatible-sequence-and-quality error message infix, the
 * offending quality string's length, the fourth
 * incompatible-sequence-and-quality error message infix, the offending input
 * line, and the incompatible-sequence-and-quality error message suffix, to
 * obtain the full incompatible-sequence-and-quality error message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID1=
  " (length: ";


/**
 * @brief Second incompatible-sequence-and-quality error message infix.
 *
 * This string constant will be appended to the
 * incompatible-sequence-and-quality error message prefix, the offending
 * sequence, the first incompatible-sequence-and-quality error message infix,
 * and the offending sequence's length, and appended by the offending quality
 * string, the third incompatible-sequence-and-quality error message infix, the
 * offending quality string's length, the fourth
 * incompatible-sequence-and-quality error message infix, the offending input
 * line, and the incompatible-sequence-and-quality error message suffix, to
 * obtain the full incompatible-sequence-and-quality error message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID2=
  ")\n"
  "  quality:  ";


/**
 * @brief Third incompatible-sequence-and-quality error message infix.
 *
 * This string constant will be appended to the
 * incompatible-sequence-and-quality error message prefix, the offending
 * sequence, the first incompatible-sequence-and-quality error message infix,
 * the offending sequence's length, the second
 * incompatible-sequence-and-quality error message infix, and the offending
 * quality string, and appended by the offending quality string's length, the
 * fourth incompatible-sequence-and-quality error message infix, the offending
 * input line, and the incompatible-sequence-and-quality error message suffix,
 * to obtain the full incompatible-sequence-and-quality error message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID3=
  " (length: ";


/**
 * @brief Fourth incompatible-sequence-and-quality error message infix.
 *
 * This string constant will be appended to the
 * incompatible-sequence-and-quality error message prefix, the offending
 * sequence, the first incompatible-sequence-and-quality error message infix,
 * the offending sequence's length, the second
 * incompatible-sequence-and-quality error message infix, the offending quality
 * string, the third incompatible-sequence-and-quality error message infix, and
 * the offending quality string's length, and appended by the offending input
 * line, and the incompatible-sequence-and-quality error message suffix, to
 * obtain the full incompatible-sequence-and-quality error message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID4=
  ")\n";


/**
 * @brief Fourth incompatible-sequence-and-quality error message suffix.
 *
 * This string constant will be appended to the
 * incompatible-sequence-and-quality error message prefix, the offending
 * sequence, the first incompatible-sequence-and-quality error message infix,
 * the offending sequence's length, the second
 * incompatible-sequence-and-quality error message infix, the offending quality
 * string, the third incompatible-sequence-and-quality error message infix, and
 * the offending quality string's length, the fourth
 * incompatible-sequence-and-quality error message infix, and the offending
 * input line, to obtain the full incompatible-sequence-and-quality error
 * message.
 */
static const char *const INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_POST=
  "\n";

/**
 * @brief Incompatible-CIGAR-and-MD error message.
 *
 * This string constant contains the to the incompatible-CIGAR-and-MD error
 * message.
 */
static const char *const INCOMPATIBLE_CIGAR_AND_MD_ERROR_MESSAGE=
  "ERROR: Incompatible CIGAR string and MD tag combination!\n";


/**
 * @brief Invalid-nucleotide error message prefix.
 *
 * This string constant will be appended by the offending nucleotide and the
 * invalid-nucleotide error message suffix to obtain the full
 * invalid-nucleotide error message.
 */
static const char *const INVALID_NUCLEOTIDE_ERROR_MESSAGE_PRE=
  "ERROR: Invalid input!\n" \
  "  Invalid nucleotide: ";


/**
 * @brief Invalid-nucleotide error message suffix.
 *
 * This string constant will be appended to the invalid-nucleotide error
 * message prefix and the offending nucleotide to obtain the full
 * invalid-nucleotide error message.
 */
static const char *const INVALID_NUCLEOTIDE_ERROR_MESSAGE_POST=
  "\n";


/*************
 * functions *
 *************/

/**
 * @brief Print too-many-arguments error message.
 *
 * This function prints the too-many-arguments usage error message to STDERR.
 * This error message consist of the usage error message, the
 * too-many-arguments error message, the usage message, and the help hint
 * message.
 *
 * @param cmd Command used to call BAMam.
 * @param argc Argument count of BAMam call (incl. `argv[0]`).
 * @param argc Maximum valid argument count (incl. `argv[0]`).
 */
void
print_too_many_arguments_error(const char const *cmd,
                               const unsigned int argc,
                               const unsigned int max);


/**
 * @brief Print invalid-flag error message.
 *
 * This function prints the invalid-flag usage error message to STDERR.
 * This error message consist of the usage error message, the invalid-flag
 * error message, the usage message, and the help hint message.
 *
 * @param cmd Command used to call BAMam.
 * @param flag Invalid flag passed to BAMam.
 */
void
print_invalid_flag_error(const char const *cmd, const char const *flag);


/**
 * @brief Print help message.
 *
 * This function prints the help message to STDOUT.
 *
 * @param cmd Command used to call BAMam.
 */
void
print_help(const char const *cmd);


/**
 * @brief Print version.
 *
 * This function prints the version number to STDOUT.
 */
void
print_version();


/**
 * @brief Print too-few-fields invalid-line error message.
 *
 * This function prints the too-few-fields invalid-line error message to
 * STDERR.
 * This error message consist of the invalid line error message and the
 * too-few-fields error message.
 *
 */
void
print_too_few_fields_error();


/**
 * @brief Print no-SAM-flag-field error message.
 *
 * This function prints the no-SAM-flag-field error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_no_sam_flag_field_error(const char const *line);


/**
 * @brief Print SAM-flag-field-last error message.
 *
 * This function prints the SAM-flag-field-last error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_sam_flag_field_last_error(const char const *line);


/**
 * @brief Print invalid-SAM-flag error message.
 *
 * This function prints the invalid-SAM-flag error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_invalid_sam_flag_error(const char const *sam_flag_start,
                             const size_t sam_flag_len,
                             const char const *line);


/**
 * @brief Print no-CIGAR-field error message.
 *
 * This function prints the no-CIGAR-field error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_no_cigar_field_error(const char const *line);


/**
 * @brief Print CIGAR-field-last error message.
 *
 * This function prints the CIGAR-field-last error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_cigar_field_last_error(const char const *line);


/**
 * @brief Print no-sequence-field error message.
 *
 * This function prints the no-sequence-field error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_no_seq_field_error(const char const *line);


/**
 * @brief Print sequence-field-last error message.
 *
 * This function prints the sequence-field-last error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_seq_field_last_error(const char const *line);


/**
 * @brief Print no-tag error message.
 *
 * This function prints the no-tag error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_no_tag_error(const char const *line);


/**
 * @brief Print no-MD-tag error message.
 *
 * This function prints the no-MD-tag error message to STDERR.
 *
 * @param line Offending input line.
 */
void
print_no_md_tag_error(const char const *line);


/**
 * @brief Print missing-CIGAR-count error message.
 *
 * This function prints the missing-CIGAR-count error message to STDERR.
 *
 * @param cigar_suffix_start Beginning of offending CIGAR string suffix.
 * @param cigar_suffix_len Length of offending CIGAR string suffix.
 */
void
print_missing_cigar_count_error(const char const *cigar_suffix_start,
                                const size_t cigar_suffix_len);


/**
 * @brief Print missing-CIGAR-character error message.
 *
 * This function prints the missing-CIGAR-character error message to STDERR.
 *
 * @param cigar_count Offending CIGAR count.
 */
void
print_missing_cigar_char_error(const size_t cigar_count);


/**
 * @brief Print invalid-CIGAR-character error message.
 *
 * This function prints the invalid-CIGAR-character error message to STDERR.
 *
 * @param cigar_suffix_start Beginning of offending CIGAR string suffix.
 * @param cigar_suffix_len Length of offending CIGAR string suffix.
 */
void
print_invalid_cigar_char_error(const char const *cigar_suffix_start,
                               const size_t cigar_suffix_len);

/**
 * @brief Print missing-MD-match-count error message.
 *
 * This function prints the missing-MD-match-count error message to STDERR.
 *
 * @param md_suffix_start Beginning of offending MD tag suffix.
 * @param md_suffix_len Length of offending MD tag suffix.
 */
void
print_missing_md_match_count_error(const char const *md_suffix_start,
                                   const size_t md_suffix_len);


/**
 * @brief Print invalid-MD-mismatch-nucleotide error message.
 *
 * This function prints the invalid-MD-mismatch-nucleotide error message to
 * STDERR.
 *
 * @param md_suffix_start Beginning of offending MD tag suffix.
 * @param md_suffix_len Length of offending MD tag suffix.
 */
void
print_invalid_md_mismatch_nucleotide_error(const char const *md_suffix_start,
                                           const size_t md_suffix_len);


/**
 * @brief Print missing-MD-deletion-nucleotide error message.
 *
 * This function prints the missing-MD-deletion-nucleotide error message to
 * STDERR.
 *
 * @param md_suffix_start Beginning of offending MD tag suffix.
 * @param md_suffix_end End offending MD tag suffix.
 */
void
print_missing_md_deletion_nucleotide_error(const char const *md_suffix_start,
                                           const char const *md_suffix_end);


/**
 * @brief Print invalid-MD-deletion-nucleotide error message.
 *
 * This function prints the invalid-MD-deletion-nucleotide error message to
 * STDERR.
 *
 * @param md_suffix_start Beginning of offending MD tag suffix.
 * @param md_suffix_len Length of offending MD tag suffix.
 */
void
print_invalid_md_deletion_nucleotide_error(const char const *md_suffix_start,
                                           const size_t md_suffix_len);


/**
 * @brief Print incompatible-sequence-and-quality error message.
 *
 * This function prints the incompatible-sequence-and-quality error message to
 * STDERR.
 *
 * @param seq_start Beginning of offending sequence.
 * @param seq_len Length of offending sequence.
 * @param qual_start Beginning of offending quality string.
 * @param qual_len Length of offending quality string.
 * @param line Offending input line.
 */
void
print_incompatible_sequence_and_quality_error(const char const *seq_start,
                                              const size_t seq_len,
                                              const char const *qual_start,
                                              const size_t qual_len,
                                              const char const *line);


/**
 * @brief Print incompatible-CIGAR-and-sequence error message.
 *
 * This function prints the incompatible-CIGAR-and-sequence error message to
 * STDERR.
 *
 */
void
print_incompatible_cigar_and_sequence_error();


/**
 * @brief Print incompatible-CIGAR-and-MD error message.
 *
 * This function prints the incompatible-CIGAR-and-MD error message to STDERR.
 *
 */
void
print_incompatible_cigar_and_md_error();


/**
 * @brief Print invalid-nucleotide error message.
 *
 * This function prints the invalid-nucleotide error message to STDERR.
 *
 */
void
print_invalid_nucleotide_error(const char nucleotide);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_MESSAGES_H */
