/*****************************************************************************
 * BAMam - BAM file annotation with mutations                                *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam main source file.
 *
 * This is the main source file for BAMam, a tool to annotate BAM files with
 * the mutations occurring in each read.
 * BAMam is an attempt to speed-up a pipeline doing the same using `awk`.
 * Also, it is used as a 'toy example' on GNU-style C development.
 *
 * @file main.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-07 - 2019-03-04
 */


/************
 * includes *
 ************/

/*
 * Include BAMam error code definition to get the following error codes:
 * - `UNKNOWN_ERROR` to return on unexpected failure (should never happen).
 */
#include <errors.h>

/*
 * Include BAMam argument parsing library to get the following function:
 * - `parse_args` to parse command line args.
 */
#include <args.h>

/*
 * Include BAMam core library to get the following function:
 * - `bamam` to run the BAMam core algorithm.
 */
#include <bamam.h>


/*****************
 * main function *
 *****************/

/**
 * @brief BAMam main function
 *
 * This is the main function of BAMam that is invoked when calling BAMam.
 * See @f errors.h for the error code definition.

 * @return `0` on success, an error code otherwise.
 */
int
main(int argc, char **argv)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the return status with a special error code to catch any
   * potentially unhandled error.
   */
  int ret = UNKNOWN_ERROR;


  /********************
   * argument parsing *
   ********************/

  /*
   * Check if command line arguments are valid and exit early if needed.
   * Note: The C standard defines `argc` and `argv` as non-`const` but since
   * `parse_args` does not need to change its parameters (and therefore should
   * not be able to do so), it was defined with `const` parameters. While the
   * `int` type of `argc` can be implicitly casted to `const int`, this does
   * not hold true for `argv`, it being a pointer type. Thus, the explicit
   * casting of the main function's `char * *` type `argv` to
   * `const char * const *` is required to avoid compiler warnings.
   */
  const unsigned char keep_going =
    parse_args(argc, (const char * const *) argv, &ret);
  if(!keep_going)
  {
    return ret;
  }


  /******************
   * core algorithm *
   ******************/

  /*
   * This part of the code is only reached if BAMam was called in a valid way
   * instructing it to actually run its core algorithm.
   */

  /*
   * Run the BAMam core algorithm.
   */
  ret = bamam();


  /****************
   * finalization *
   ****************/

  /*
   * Pass up the return value given by the core algorithm function.
   */
  return ret;
}
