/*****************************************************************************
 * BAMam SAM parsing library source file                                     *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam SAM parsing library source file.
 *
 * This file contains the implementation of the BAMam SAM parsing functions
 * defined in @f sam.h.
 *
 * @file sam.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-29 - 2019-04-25
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <sam.h>

/*
 * Include BAMam error code definition to get the following error codes:
 * - `UNKNOWN_LINE_PARSING_ERROR` to return when parsing a line fails in an
 *   unexpected way (should never happen).
 * - `TOO_FEW_FIELDS_ERROR` to return when an input line has too few fields.
 * - `NO_ERROR` to return upon success.
 */
#include <errors.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify an invariant at runtime.
 */
#include <assert.h>

/*
 * Include standard string library to get the following functions:
 * - `strchr` to search a (null-terminated) string for a given character.
 * - `strncmp` to compare the first N characters of two (null-terminated)
 *   strings.
 */
#include <string.h>

/*
 * Include BAMam message printing library to get the following functions:
 * - `print_too_few_fields_error` to print the too-few-fields invalid-line
 *   error message to STDERR.
 * - `print_no_sam_flag_field_error` to print the no-SAM-flag-field error
 *   message to STDERR.
 * - `print_sam_flag_field_last_error` to print the SAM-flag-field-last error
 *   message to STDERR.
 * - `print_invalid_sam_flag_error` to print the invalid-SAM-flag error message
 *   to STDERR.
 * - `print_no_cigar_field_error` to print the no-CIGAR-field error message to
 *   STDERR.
 * - `print_cigar_field_last_error` to print the CIGAR-field-last error message
 *   to STDERR.
 * - `print_no_seq_field_error` to print the no-sequence-field error message to
 *   STDERR.
 * - `print_seq_field_last_error` to print the sequence-field-last error
 *   message to STDERR.
 * - `print_no_tag_error` to print the no-tag error message to STDERR.
 * - `print_no_md_tag_error` to print the no-MD-tag error message to STDERR.
 */
#include <messages.h>

/*
 * Include C standard library to get the following function:
 * - `strtoul` to parse an unsigned (long) integer from a string.
 */
#include <stdlib.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Move cursor to the next field.
 *
 * This function moves a pointer from the beginning of a field within a line to
 * the beginning of the next field.
 * If there is another field left to move the cursor, not only is the location
 * of the beginning of the next field returned, but also is the no-error code
 * written to the location pointed to by @p status to indicate success. If this
 * is not the case the other hand, an error message is printed to STDERR, an
 * error code is written to the location pointed to by @p status, and the null
 * pointer is returned.
 * See @f errors.h for the error code definition.
 * If @p status is the null pointer, the moved cursor is still returned (or the
 * null pointer upon error), but no error message will be printed.
 *
 * @param cursor Location of the beginning of a field of a line that should be
 *               moved to the beginning of the next field of that line.
 * @param status Location write the error code to.
 * @return One if successful, zero otherwise.
 */
const char *
seek_to_next_field(const char *cursor, int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define field separator to improve code readability.
   */
  const char FIELD_SEPARATOR = '\t';


  /********
   * seek *
   ********/

  /*
   * Move the cursor to the next field separator.
   * Exit early reporting error if requested if no field separator is found.
   */
  cursor = strchr(cursor, FIELD_SEPARATOR);
  if(!cursor)
  {
    if(status)
    {
      print_too_few_fields_error();
      *status = TOO_FEW_FIELDS_ERROR;
    }
    return cursor;
  }


  /****************
   * finalization *
   ****************/

  /* As the cursor points at a field separator and the last character of the
   * line (before the terminating null character) must be the final newline,
   * the cursor cannot point at the end of the line yet. Thus, it can safely be
   * moved one more position to now point to the beginning of the next field.
   * Note: This check could fail if the SAM line actually contained a literal
   * null character. Since this possibility is not taken into account by the
   * (current) code it should make BAMam fail in a controlled way rather than
   * resulting in undefined behaviour.
   */
  assert((++cursor)[0] != '\0');

  /*
   * Return the new cursor position reporting success if requested if no error
   * occurred.
   */
  if(status){
    *status = NO_ERROR;
  }
  return cursor;
}


/**
 * @brief Move cursor by N fields.
 *
 * This function moves a pointer from the beginning of a field within a line to
 * the beginning of the Nth next field.
 * If there are sufficiently many fields left to move the cursor, not only is
 * its moved location returned, but also is the no-error code written to the
 * location pointed to by @p status to indicate success. If this is not the
 * case the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status, and the null pointer is
 * returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning of a field of a line that should be
 *               moved to the beginning of the field @p fields fields down that
 *               line.
 * @param fields Number of fields to move @p cursor down the line.
 * @return Zero if successful, an error code otherwise.
 */
const char *
seek_n_fields_down(const char *cursor,
                   unsigned short int fields,
                   int *status)
{
  /*
   * Move the cursor one field at a time as long as there is a next field to
   * move to until the requested number of fields has been passed over and pass
   * up the new cursor position (is set to `NULL` upon error).
   * Note: `seek_to_next_field` takes care of printing an error message to
   * STDERR if necessary.
   */
  while(cursor && fields--)
  {
    cursor = seek_to_next_field(cursor, status);
  }
  return cursor;
}


/**
 * @brief Move cursor from beginning to SAM flag field.
 *
 * This function moves a pointer from the beginning of a line to the beginning
 * of the SAM flag field.
 * If there are sufficiently many fields left to move the cursor, not only is
 * its moved location returned, but also is the no-error code written to the
 * location pointed to by @p status to indicate success. If this is not the
 * case the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status, and the null pointer is
 * returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning of a line.
 * @return Zero if successful, an error code otherwise.
 */
const char *
seek_from_beginning_to_sam_flag_field(const char *cursor, int *status)
{
  /******************
   * initialization *
   ******************/

  /*
   * Define (zero based!) index of SAM flag field within valid SAM input line
   * to improve code readability.
   */
  const unsigned short int SAM_FLAG_FIELD = 1;


  /********
   * seek *
   ********/

  /*
   * Move the cursor as many fields down, as the SAM flag field is from the
   * beginning of a valid SAM input line and pass up the new cursor position
   * (is set to `NULL` upon error).
   * Note: `seek_n_fields` takes care of printing an error message to STDERR if
   * necessary.
   */
  return seek_n_fields_down(cursor, SAM_FLAG_FIELD, status);
}


/**
 * @brief Move cursor from beginning of SAM flag field to beginning to CIGAR
 * field.
 *
 * This function moves a pointer from the beginning of the field after the SAM
 * flag field to the beginning of the CIGAR field.
 * If there are sufficiently many fields left to move the cursor, not only is
 * its moved location returned, but also is the no-error code written to the
 * location pointed to by @p status to indicate success. If this is not the
 * case the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status, and the null pointer is
 * returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning of a line.
 * @return Zero if successful, an error code otherwise.
 */
const char *
seek_from_after_sam_flag_to_cigar_field(const char *cursor, int *status)
{
  /******************
   * initialization *
   ******************/

  /*
   * Define (zero based!) index of SAM flag field within valid SAM input line
   * to improve code readability.
   */
  const unsigned short int SAM_FLAG_FIELD = 1;

  /*
   * Define (zero based!) index of CIGAR field within valid SAM input line
   * to improve code readability.
   */
  const unsigned short int CIGAR_FIELD = 5;


  /********
   * seek *
   ********/

  /*
   * The cursor cannot be moved backwards. Thus, to be able move it from the
   * field after the SAM flag field to the CIGAR field, the CIGAR field must
   * be downstream of the field after the SAM flag field.
   */
  assert(CIGAR_FIELD > SAM_FLAG_FIELD + 1);

  /*
   * Move the cursor as many fields down, as the CIGAR field is from the
   * beginning of the field after the SAM flag field and pass up the new cursor
   * position (is set to `NULL` upon error).
   * Note: `seek_n_fields` takes care of printing an error message to STDERR if
   * necessary.
   */
  cursor = seek_n_fields_down(cursor, CIGAR_FIELD - SAM_FLAG_FIELD - 1,
                              status);
  return cursor;
}


/**
 * @brief Move cursor from after CIGAR to sequence field.
 *
 * This function moves a pointer from the beginning of the field after the
 * CIGAR field of a line to the beginning of the sequence field.
 * If there are sufficiently many fields left to move the cursor, not only is
 * its moved location returned, but also is the no-error code written to the
 * location pointed to by @p status to indicate success. If this is not the
 * case the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status, and the null pointer is
 * returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning of the field after the CIGAR field.
 * @return Zero if successful, an error code otherwise.
 */
const char *
seek_from_after_cigar_to_seq_field(const char *cursor, int *status)
{
  /******************
   * initialization *
   ******************/

  /*
   * Define (zero based!) index of CIGAR field within valid SAM input line
   * to improve code readability.
   */
  const unsigned short int CIGAR_FIELD = 5;

  /*
   * Define (zero based!) index of sequence field within valid SAM input line
   * to improve code readability.
   */
  const unsigned short int SEQ_FIELD = 9;


  /********
   * seek *
   ********/

  /*
   * The cursor cannot be moved backwards. Thus, to be able move it from the
   * field after the CIGAR field to the sequence field, the sequence field must
   * be downstream of the CIGAR field.
   */
  assert(SEQ_FIELD > CIGAR_FIELD + 1);

  /*
   * Move the cursor as many fields down, as the sequence field is from the
   * beginning of the field after the CIGAR field in a valid SAM input line and
   * pass up the new cursor position (is set to `NULL` upon error).
   * Note: `seek_n_fields` takes care of printing an error message to STDERR if
   * necessary.
   */
  cursor = seek_n_fields_down(cursor, SEQ_FIELD - CIGAR_FIELD - 1, status);
  return cursor;
}


/**
 * @brief Check if tag is MD tag and move cursor past prefix.
 *
 * This function checks for a given tag if it is an MD tag and if so moves the
 * cursor from the beginning of the tag to just behind the prefix.
 *
 * @param cursor Location of pointer to beginning of tag to check.
 * @return One if @p tag is an MD tag, zero otherwise.
 */
unsigned char
skip_md_tag_prefix(const char **cursor)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define return values to improve code readability.
   */
  const unsigned char TAG_IS_NOT_MD_TAG = 0;
  const unsigned char TAG_IS_MD_TAG = 1;

  /*
   * Define prefix and length of an MD tag to improve code readability.
   */
  const char *const MD_TAG_PREFIX = "MD:Z:";
  const size_t MD_TAG_PREFIX_LENGTH = 5;


  /**********
   * MD tag *
   **********/

  /*
   * Exit early moving the cursor behind the prefix and reporting the tag to be
   * an MD tag if it starts with the MD tag prefix.
   */
  if(!strncmp(*cursor, MD_TAG_PREFIX, MD_TAG_PREFIX_LENGTH))
  {
    *cursor += MD_TAG_PREFIX_LENGTH;
    return TAG_IS_MD_TAG;
  }


  /**************
   * non-MD tag *
   **************/

  /*
   * This part of the code is only reached if the tag to check does not start
   * with the MD tag prefix and therefore is not an MD tag.
   */

  /*
   * Report the line is not a SAM header line for any other line.
   */
  return TAG_IS_NOT_MD_TAG;
}


/**
 * @brief Move cursor from first tag to MD tag value.
 *
 * This function moves a pointer from the beginning of the first tag of a line
 * to the beginning of the MD tag's value (skipping its prefix).
 * If there is an MD tag, not only is the location of the beginning of its
 * value (skipping the prefix) returned, but also is the no-error code written
 * to the location pointed to by @p status to indicate success. If this is not
 * the case the other hand, an error message is printed to STDERR, an error
 * code is written to the location pointed to by @p status, and the null
 * pointer is returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning of the first tag.
 * @return Zero if successful, an error code otherwise.
 */
const char *
seek_from_first_to_md_tag_val(const char *cursor, int *status)
{
  /*
   * Move the cursor one field at a time as long as there is a next field to
   * move to until it points to an MD tag and pass up the new cursor position
   * (is set to `NULL` upon error).
   * Note: `seek_to_next_field` takes care of printing an error message to
   * STDERR if necessary.
   */
  while(cursor && !(skip_md_tag_prefix(&cursor)))
  {
    cursor = seek_to_next_field(cursor, status);
  }
  return cursor;
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Check if line is header line.
 */
unsigned char
is_header_line(const char *const line)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define return values to improve code readability.
   */
  const unsigned char LINE_IS_NOT_HEADER = 0;
  const unsigned char LINE_IS_HEADER = 1;

  /*
   * Define character and its position in the line (zero-based) indicating it
   * is a SAM header line to improve code readability.
   */
  const char HEADER_CHAR = '@';
  const size_t HEADER_CHAR_POS = 0;


  /****************
   * header lines *
   ****************/

  /*
   * Exit early reporting the line is a SAM header if it has the defining
   * character at the correct position.
   */
  if(line[HEADER_CHAR_POS] == HEADER_CHAR)
  {
    return LINE_IS_HEADER;
  }


  /********************
   * non-header lines *
   ********************/

  /*
   * This part of the code is only reached if the line to check does not have
   * the defining character at the correct position and therefore is not a SAM
   * header line.
   */

  /*
   * Report the line is not a SAM header line for any other line.
   */
  return LINE_IS_NOT_HEADER;
}


/**
 * Parse single non-header line checking its validity for the BAMam.
 */
unsigned char
parse_line(const char *const line, size_t len, int *status,
           unsigned char *const unmapped_flag,
           unsigned char *const reverse_flag,
           const char **cigar_start, size_t *cigar_len,
           const char **seq_start, size_t *seq_len,
           const char **qual_start, size_t *qual_len,
           const char **md_start, size_t *md_len)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_LINE_PARSING_ERROR;

  /*
   * Define return values to improve code readability.
   */
  const unsigned char FAILURE = 0;
  const unsigned char SUCCESS = 1;

  /*
   * Define SAM flag base base to improve code readability.
   */
  const unsigned char BASE_SAM_FLAG = 10;

  /*
   * Define unmapped SAM flag to improve code readability.
   */
  const unsigned short int SAM_FLAG_UNMAPPED = 4;

  /*
   * Define reverse-mapping SAM flag to improve code readability.
   */
  const unsigned short int SAM_FLAG_REVERSE = 16;

  /*
   * Initialize SAM flag field start and end pointers and value.
   */
  const char *sam_flag_start = NULL;
  char *sam_flag_end = NULL;
  unsigned short int sam_flag = 0;

  /*
   * Initialize CIGAR field start pointer and length.
   */
  *cigar_start = NULL;
  *cigar_len = 0;

  /*
   * Initialize sequence field start pointer and length.
   */
  *seq_start = NULL;
  *seq_len = 0;

  /*
   * Initialize MD tag start pointer and length.
   */
  *md_start = NULL;
  *md_len = 0;

  /*
   * Initialize parsing cursor to the beginning of the line.
   */
  const char *cursor = line;


  /************
   * SAM flag *
   ************/

  /*
   * Try to move cursor until beginning of the SAM flag field and exit early
   * reporting error if no SAM flag field was found.
   */
  cursor = seek_from_beginning_to_sam_flag_field(cursor, status);
  if(!cursor)
  {
    print_no_sam_flag_field_error(line);
    return FAILURE;
  }

  /*
   * Store beginning of the SAM flag field, move the cursor to the beginning of
   * the following field, and exit early reporting error if the SAM flag field
   * turns out to be the last field on the line.
   */
  sam_flag_start = cursor;
  cursor = seek_to_next_field(cursor, status);
  if(!cursor)
  {
    print_sam_flag_field_last_error(line);
    return FAILURE;
  }


  /*****************
   * unmapped flag *
   *****************/

  /*
   * This part of the code is only reached when the cursor was successfully
   * positioned at the beginning of the field after the SAM flag field.
   */

  /*
   * Parse SAM flag field into unsigned integer and exit early reporting error
   * if part of it cannot be intepreted as such.
   * Note: The end of the parsed field should be one character *before* the
   * current cursor postion to account for the field separator between the SAM
   * flag field and the following field currently pointed to by the cursor.
   */
  sam_flag = strtoul(sam_flag_start, &sam_flag_end, BASE_SAM_FLAG);
  if(sam_flag_end != (cursor - 1))
  {
    *status = INVALID_SAM_FLAG_ERROR;
    const size_t sam_flag_len = cursor - sam_flag_start - 1;
    print_invalid_sam_flag_error(sam_flag_start, sam_flag_len, line);
    return FAILURE;
  }

  /*
   * Probe the corresponding bit of the SAM flag to determine if the alignment
   * corresponds to an unmapped read.
   */
  *unmapped_flag = ((sam_flag & SAM_FLAG_UNMAPPED) > 0);


  /**********
   * strand *
   **********/

  /*
   * Probe the corresponding bit of the SAM flag to determine if the alignment
   * corresponds to a reverse-mapped read.
   */
  *reverse_flag = ((sam_flag & SAM_FLAG_REVERSE) > 0);


  /***************
   * CIGAR field *
   ***************/

  /*
   * This part of the code is only reached when the cursor was successfully
   * positioned at the beginning of the field after the (valid) SAM flag field.
   */

  /*
   * Try to move cursor until beginning of the CIGAR field and exit early
   * reporting error if no CIGAR field was found.
   */
  cursor = seek_from_after_sam_flag_to_cigar_field(cursor, status);
  if(!cursor)
  {
    print_no_cigar_field_error(line);
    return FAILURE;
  }

  /*
   * Store beginning of the CIGAR field, move the cursor to the beginning of
   * the following field, and exit early reporting error if the CIGAR field
   * turns out to be the last field on the line.
   */
  *cigar_start = cursor;
  cursor = seek_to_next_field(cursor, status);
  if(!cursor)
  {
    print_cigar_field_last_error(line);
    return FAILURE;
  }

  /*
   * Calculate the CIGAR length as the distance between the beginning of the
   * CIGAR field and that of the next one and subtract one character for the
   * field separator between those two fields.
   */
  *cigar_len = cursor - *cigar_start - 1;


  /******************
   * sequence field *
   ******************/

  /*
   * This part of the code is only reached when the cursor was successfully
   * positioned at the beginning of the field after the CIGAR field.
   */

  /*
   * Try to move cursor until beginning of the sequence field and exit early
   * reporting error if no sequence field was found.
   */
  cursor = seek_from_after_cigar_to_seq_field(cursor, status);
  if(!cursor)
  {
    print_no_seq_field_error(line);
    return FAILURE;
  }

  /*
   * Store beginning of the sequence field, move the cursor to the beginning of
   * the following field, and exit early reporting error if the sequence field
   * turns out to be the last field on the line.
   */
  *seq_start = cursor;
  cursor = seek_to_next_field(cursor, status);
  if(!cursor)
  {
    print_seq_field_last_error(line);
    return FAILURE;
  }

  /*
   * Calculate the sequence length as the distance between the beginning of the
   * sequence field and that of the next one and subtract one character for the
   * field separator between those two fields.
   */
  *seq_len = cursor - *seq_start - 1;


  /*****************
   * quality field *
   *****************/

  /*
   * This part of the code is only reached when the cursor was successfully
   * positioned at the beginning of the field after the sequence field, thus
   * the quality field.
   */

  /*
   * Store beginning of the quality field, move the cursor to the beginning of
   * the following field, and exit early reporting error if the quality field
   * turns out to be the last field on the line.
   */
  *qual_start = cursor;
  cursor = seek_to_next_field(cursor, status);
  if(!cursor)
  {
    print_no_tag_error(line);
    return FAILURE;
  }

  /*
   * Calculate the quality length as the distance between the beginning of the
   * quality field and that of the next one and subtract one character for the
   * field separator between those two fields.
   */
  *qual_len = cursor - *qual_start - 1;


  /**********
   * MD tag *
   **********/

  /*
   * Exit early reporting success if the alignemnt corresponds to an unmapped
   * read (no MD tags expected).
   * Note: The MD tag start and length were initialized to NUL and zero,
   * respectively, and should not have changed since then. Instead of blindly
   * relying on this or setting them simply to improve code readability,
   * assertions are used to fail in a controlled manner if anything ever
   * changes these invariants.
   */
  if(*unmapped_flag)
  {
    assert(*md_start == NULL);
    assert(*md_len == 0);
    *status = NO_ERROR;
    return SUCCESS;
  }

  /*
   * This part of the code is only reached when the cursor was successfully
   * positioned at the beginning of the field after the quality field, thus the
   * first tag field and the alignemnt corresponds to a mapped read.
   */

  /*
   * Try to move cursor until beginning of the MD tag and exit early reporting
   * error if no MD tag was found.
   */
  cursor = seek_from_first_to_md_tag_val(cursor, status);
  if(!cursor)
  {
    print_no_md_tag_error(line);
    assert(*status != NO_ERROR);
    return FAILURE;
  }

  /*
   * Store beginning of the MD tag, try to move the cursor to the beginning of
   * the following tag (ignoring error), and move it to the end of the line if
   * it the MD tag turns out to be the last field on the line.
   */
  *md_start = cursor;
  cursor = seek_to_next_field(cursor, NULL);
  if(!cursor)
  {
    cursor = line + len;
  }

  /*
   * Calculate the MD tag length as the distance between the beginning of the
   * MD tag and that of the next one and subtract one character for the field
   * separator between those two fields.
   * Note: If the MD tag is the last field of the line, instead of a field
   * separator it will be followed by the line's final newline. Thus,
   * subtracting one yields the correct length either way.
   */
  *md_len = cursor - *md_start - 1;


  /****************
   * finalization *
   ****************/

  /*
   * This part of the code should only be reached if no error occurred while
   * parsing the line. Thus, parsing success can be reported safely.
   */
  assert(*status == NO_ERROR);
  return SUCCESS;
}


/*
 * Decode a quality character to the corresponding score.
 */
unsigned short int
qual_char2score(const char qual_char)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define Phred offset used in (Phred+33/Sanger/Illumina 1.8+) encoding to
   * improve code readability.
   */
  const unsigned short int PHRED_SHIFT = 33;


  /******************
   * initialization *
   ******************/

  /*
   * Decode character to integer value and undo the Phred shift.
   */
  return (((unsigned short int) qual_char) - PHRED_SHIFT);
}
