/*****************************************************************************
 * BAMam CIGAR parsing library source file                                   *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam CIGAR parsing library source file.
 *
 * This file contains the implementation of the BAMam CIGAR parsing functions
 * defined in @f cigar.h.
 *
 * @file cigar.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-29 - 2019-04-09
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <cigar.h>

/*
 * Include bamam parsing library to get the following function:
 * - `update_count` to update count.
 */
#include <parsing.h>

/*
 * Include BAMam error code definition to get the following error codes:
 * - `NO_ERROR` to return upon success.
 * - `UNKNOWN_CIGAR_PARSING_ERROR` to return when parsing a CIGAR string fails
 *   in an unexpected way (should never happen).
 * - `INVALID_CIGAR_ERROR` to return when a CIGAR string is invalid.
 * - `UNKNOWN_CIGAR_PROCESSING_ERROR` to return when processing a CIGAR string
 *   fails in an unexpected way (should never happen).
 */
#include <errors.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify an invariant at runtime.
 */
#include <assert.h>

/*
 * Include BAMam message printing library to get the following functions:
 * - `print_missing_cigar_count_error` to print the missing-CIGAR-count error
 *   message to STDERR.
 * - `print_missing_cigar_char_error` to print the missing-CIGAR-character
 *   error message to STDERR.
 * - `print_invalid_cigar_char_error` to print the invalid-CIGAR-character
 *   error message to STDERR.
 */
#include <messages.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Check if character is valid CIGAR character.
 *
 * This function checks for a single character, if it is a valid (non-digit)
 * CIGAR character.
 *
 * @param character Character to check.
 * @return One if @p character is a valid (non-digit) CIGAR character, zero
 *         otherwise.
 */
unsigned char
is_valid_cigar_char(const char character)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define all valid (non-digit, non-deletion, non-skipped, non-soft-clipped,
   * non-insertion) CIGAR characters to improve code readability.
   * Source: http://bioinformatics.cvr.ac.uk/blog/tag/cigar-string/
   */
  const char CIGAR_CHAR_HARD_CLIPPING = 'H';
  const char CIGAR_CHAR_MATCH = '=';
  const char CIGAR_CHAR_MATCH_OR_MISSMATCH = 'M';
  const char CIGAR_CHAR_MISSMATCH = 'X';
  const char CIGAR_CHAR_PADDING = 'P';


  /*********
   * check *
   *********/

  /*
   * Compare to all valid (non-digit) CIGAR characters and report success on
   * the first match.
   * Note: Due to C's short-circuit evaluation, performance of this statement
   * improves if the comparisons are ordered from most to least common. This is
   * especially true since the no-match case can only be reached once on
   * invalid input before erroring out.
   */
  return (   character == CIGAR_CHAR_MATCH_OR_MISSMATCH
          || is_skipping_char(character)
          || is_deletion_char(character)
          || is_soft_clipping_char(character)
          || is_insertion_char(character)
          || character == CIGAR_CHAR_HARD_CLIPPING
          || character == CIGAR_CHAR_PADDING
          || character == CIGAR_CHAR_MATCH
          || character == CIGAR_CHAR_MISSMATCH);
}


/**
 * @brief Parse CIGAR entry.
 *
 * This function parses a single CIGAR entry (count followed by character)
 * based on the beginning of the entry within the CIGAR string and the
 * (remaining) length of the CIGAR string (suffix) from that position on.
 * If the given field is valid, not only is the location of the (non-digit)
 * character of the CIGAR entry returned, but also are the corresponding count
 * and remaining CIGAR string suffix length written to the locations pointed at
 * by @p count and @p len, respectively, as well as the no-error code to the
 * location pointed at by @p status to indicate success. If this is not the
 * case on the other hand, an error message is printed to STDERR, an error code
 * is written to the location pointed to by @p status to indicate the
 * invalidity of the CIGAR entry, zero is written to the locations pointed to
 * by @p count and @p len, and the null pointer is returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning (i.e. first digit) of the CIGAR
 *               entry to parse.
 * @param len Location to read/write length of the CIGAR string (suffix)
 *            pointed to by @p cursor from/to.
 * @param count Location to write the parsed CIGAR count to.
 * @param status Location to write the (non-)error code to.
 * @return Location of the end (i.e. (non-digit) character) of the CIGAR entry
 *         if valid, the null pointer otherwise.
 */
const char *
parse_cigar_entry(const char *cursor, size_t *const len, size_t *const count,
                  int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_CIGAR_PARSING_ERROR;

  /*
   * Initialize the CIGAR count with zero (empty sum).
   */
  *count = 0;


  /*********
   * count *
   *********/

  /*
   * Process the first digit and exit early reporting error if the given CIGAR
   * entry begins with a non-digit character.
   * Note: The CIGAR count was initialized to zero and should not have changed
   * when there was no digit. Instead of blindly relying on this or setting it
   * simply to improve code readability, an assertion is used to fail in a
   * controlled manner if anything ever changes this invariant.
   */
  if(!update_count(count, *cursor))
  {
    *status = INVALID_CIGAR_ERROR;
    print_missing_cigar_count_error(cursor, *len);
    *len = 0;
    assert(*count == 0);
    return NULL;
  }

  /*
   * This part of the code is only reached if the cursor points at a digit
   * which was already added to the CIGAR count.
   */

  /*
   * Move cursor to the second character of the CIGAR string and keep moving it
   * further down as long as there are digits left to process.
   */
  for(++cursor; --(*len) && update_count(count, *cursor); ++cursor);


  /*************
   * character *
   *************/

  /*
   * This part of the code is only reached if at least one digit was processed
   * from the beginning of the CIGAR entry, the CIGAR count was set to the
   * numeric value encoded by the processed digit(s), and either no non-digit
   * characters were found before reaching the end of the CIGAR string
   * (suffix), or the cursor points at the first non-digit character
   * encountered.
   */

  /*
   * Exit early reporting error if the given CIGAR string (suffix) consists of
   * digits only.
   * Note: The remaining length should equal zero if no non-digit character was
   * found. Instead of blindly relying on this or setting it simply to improve
   * code readability, an assertion is used to fail in a controlled manner if
   * anything ever changes this invariant.
   */
  if(!(*len))
  {
    *status = INVALID_CIGAR_ERROR;
    print_missing_cigar_char_error(*count);
    assert(*len == 0);
    *count = 0;
    return NULL;
  }

  /*
   * Exit early reporting error if the non-digit character of the CIGAR entry
   * is not a valid (non-digit) CIGAR character.
   */
  if(!is_valid_cigar_char(*cursor))
  {
    *status = INVALID_CIGAR_ERROR;
    print_invalid_cigar_char_error(cursor, *len);
    *len = 0;
    *count = 0;
    return NULL;
  }


  /****************
   * finalization *
   ****************/

  /*
   * This part of the code is only reached if the CIGAR entry consists of at
   * least one digit followed by a valid (non-digit) CIGAR character, the cigar
   * count was set to the numeric value encoded by the digits, and the cursor
   * points to the corresponding (non-digit) CIGAR character.
   */

  /*
   * Pass up the location of the end (i.e. the (non-digit) CIGAR character) of
   * the parsed CIGAR entry reporting success.
   */
  *status = NO_ERROR;
  return cursor;
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Get next CIGAR character.
 */
const char *
get_next_cigar_char(const char const **cursor, size_t *len, size_t *count,
                    int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_CIGAR_PARSING_ERROR;

  /*
   * Initialize the current CIGAR character.
   */
  const char *cigar_char = NULL;


  /***********
   * parsing *
   ***********/

  /*
   * If the current CIGAR entry was processed completely, try to parse the
   * next one.
   */
  if(!(*count))
  {
    *cursor = parse_cigar_entry(*cursor, len, count, status);
  }

  /*
   * Error out if there is no valid current CIGAR entry.
   */
  if(!(*cursor))
  {
    *len = 0;
    *count = 0;
    assert(*status != NO_ERROR);
    return NULL;
  }


  /**************
   * processing *
   **************/

  /*
   * This part of the code is only reached if a valid CIGAR entry was parsed.
   */
  assert(*count);

  /*
   * Remember the location of the current CIGAR character.
   * This is necessary to be able to return the current character for a last
   * time after moving the cursor already to the beginning of the next CIGAR
   * entry.
   */
  cigar_char = *cursor;

  /*
   * Remove the processed CIGAR character from CIGAR count and if the current
   * CIGAR entry was processed completely, move the cursor to the next one.
   */
  if(!(--(*count)))
  {
    assert(*len);
    ++(*cursor);
    --(*len);
  }


  /****************
   * finalization *
   ****************/

  /*
   * Return the location of the current CIGAR character indicating success.
   */
  *status = NO_ERROR;
  return cigar_char;
}


/*
 * Check if character is deletion CIGAR character.
 */
unsigned char
is_deletion_char(const char character)
{

  /******************
   * initialization *
   ******************/
  /*
   * Define deletion CIGAR character to improve code readability.
   * Source: http://bioinformatics.cvr.ac.uk/blog/tag/cigar-string/
   */
  const char CIGAR_CHAR_DELETION = 'D';


  /*********
   * check *
   *********/

  /*
   * Compare to deletion CIGAR character and report success on match.
   */
  return (character == CIGAR_CHAR_DELETION);
}


/*
 * Check if character is skipping CIGAR character.
 */
unsigned char
is_skipping_char(const char character)
{

  /******************
   * initialization *
   ******************/
  /*
   * Define skipping CIGAR character to improve code readability.
   * Source: http://bioinformatics.cvr.ac.uk/blog/tag/cigar-string/
   */
  const char CIGAR_CHAR_SKIPPING = 'N';


  /*********
   * check *
   *********/

  /*
   * Compare to skipping CIGAR character and report success on match.
   */
  return (character == CIGAR_CHAR_SKIPPING);
}


/*
 * Check if character is soft-clipping CIGAR character.
 */
unsigned char
is_soft_clipping_char(const char character)
{

  /******************
   * initialization *
   ******************/
  /*
   * Define soft-clipping CIGAR character to improve code readability.
   * Source: http://bioinformatics.cvr.ac.uk/blog/tag/cigar-string/
   */
  const char CIGAR_CHAR_SOFT_CLIPPING = 'S';


  /*********
   * check *
   *********/

  /*
   * Compare to soft-clipping CIGAR character and report success on match.
   */
  return (character == CIGAR_CHAR_SOFT_CLIPPING);
}


/*
 * Check if character is insertion CIGAR character.
 */
unsigned char
is_insertion_char(const char character)
{

  /******************
   * initialization *
   ******************/
  /*
   * Define insertion CIGAR character to improve code readability.
   * Source: http://bioinformatics.cvr.ac.uk/blog/tag/cigar-string/
   */
  const char CIGAR_CHAR_INSERTION = 'I';


  /*********
   * check *
   *********/

  /*
   * Compare to insertion CIGAR character and report success on match.
   */
  return (character == CIGAR_CHAR_INSERTION);
}
