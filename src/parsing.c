/*****************************************************************************
 * BAMam parsing library source file                                         *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam parsing library source file.
 *
 * This file contains the implementation of the BAMam parsing functions defined
 * in @f parsing.h.
 *
 * @file parsing.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-03-04
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <parsing.h>


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Update count.
 */
unsigned char
update_count(size_t *count, const char character)
{

  /******************
   * initialization *
   *******************/

  /*
   * Define return values to improve code readability.
   */
  const unsigned char CHAR_IS_DIGIT = 1;
  const unsigned char CHAR_IS_NOT_DIGIT = 0;

  /*
   * Define encoding if zero to improve code readability.
   */
  const unsigned char ZERO_CHAR = '0';

  /*
   * Define first non-single-digit-number to improve code readability.
   */
  const unsigned char TEN = 10;


  /************************
   * non-digit characters *
   ************************/

  /*
   * Convert the given character to its numeric value assuming it encodes a
   * digit and exit early reporting failure if it turns out not to.
   * Note: Regardless of the encoding used, all digit follow successively in
   * ascending order. Thus, for any character encoding a digit, its offset from
   * the character encoding the digit 'zero' equals its numeric values.
   * Characters encoded by higher values will end up with numeric values
   * greater or equal to ten. While characters encoded by lower values would
   * end up with negative numeric values, the unsigned nature of `digit` forces
   * them to underflow to positive values. As the numeric range that can be
   * represented by a (signed) `char` is big enough to hold the encodings of
   * all characters, so must its unsigned counterpart. Therefore, this
   * conversion essentially implements a modulo-shift on a ring large enough to
   * hold all possible values, excluding the possibility of an overlap. Thus,
   * negative values won't only be mapped to positive values, but also to
   * positive values larger than the largest positive value that can be reached
   * without integer underflow. This guarantees, that any non-digit character
   * will be assigned a unique numeric value larger or equal to ten.
   */
  const unsigned char digit = character - ZERO_CHAR;
  if(digit >= TEN)
  {
    return CHAR_IS_NOT_DIGIT;
  }


  /**********
   * digits *
   **********/

  /*
   * Shift the current count left to make room for the new digit on the
   * right-most position.
   * Note: Shifting previously processed digits left is equal to raising their
   * exponents by one. In the decimal system, this in turn is equivalent to
   * multiplying each digit by ten before adding the powers. Finally,
   * multiplying each part of a sum with the same factor yields the same result
   * as multiplying the sum (distributive law).
   */
  *count *= TEN;

  /*
   * Add new digit.
   * Note: As the new digit is shifted in from the right its exponent is zero.
   * As multiplying for a zero-power is a non-operation, all that is left to
   * account for the new digit is to add its value to the shifted sum.
   */
  *count += digit;

  /*
   * Report success.
   */
  return CHAR_IS_DIGIT;
}
