/*****************************************************************************
 * BAMam SAM parsing library header file                                     *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam SAM parsing library header file.
 *
 * This is the header file for BAMam's SAM parsing library.
 *
 * @file sam.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-28 - 2019-04-25
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_SAM_H
#define BAMAM_SAM_H


/************
 * includes *
 ************/

/*
 * Include standard definitions library to get the following type:
 * - `size_t` for unsigned integers large enough to hold any array index.
 */
#include <stddef.h>


/*************
 * functions *
 *************/

/**
 * @brief Check if line is part of the SAM header.
 *
 * This function checks for a single input line if it part of the SAM header.
 *
 * @param line Input line (incl. final newline, null-terminated) to check.
 * @return One if @p line is part of the SAM header, zero otherwise.
 */
unsigned char
is_header_line(const char *const line);


/**
 * @brief Parse single non-header line.
 *
 * This function parses a single non-header input line checking its validity
 * for BAMam. The behaviour is undefined when passing a line that satisfies
 * `is_header_line`.
 * If the line is valid, not only is one returned to indicate the validity, but
 * also is the no-error code written to the location pointed to by @p status,
 * boolean flags indicating whether or not the alignment corresponds to an
 * unmapped or reverse-mapped read are witten to the locations pointed to by
 * @p unmapped_flag and @p reverse_flag, respectively, the location of the
 * beginning of the CIGAR field is written to the location @p cigar_start
 * points to, the CIGAR field length is written to the location pointed to by
 * @p cigar_len, the location of the beginning of the sequence field is written
 * to the location @p seq_start points to, the sequence field length is written
 * to the location pointed to by @p seq_len, the location of the beginning of
 * the quality field is written to the location @p qual_start points to, the
 * quality field length is written to the location pointed to by @p qual_len,
 * the location of the beginning of the MD tag field is written to the location
 * @p md_start points to, and the MD tag field length is written to the
 * location pointed to by @p md_len.
 * If the line is invalid on the other hand, zero is returned to indicate the
 * invalidity and the corresponding error code is written to the location
 * pointed to by @p status, zero is written to the locations pointed to by
 * @p unmapped_flag and @p reverse_flag (setting the boolean flags to false),
 * the null pointer is written to the locations pointed to by @p cigar_start,
 * @p seq_start, @p qual_start and @p md_start, and zero is written to the
 * locations @p cigar_len, @p seq_len, @p qual_len and @p md_len point to.
 * See @f errors.h for the error code definition.
 *
 * @param line Non-header line (incl. final newline, null-terminated) to parse.
 * @param len Length of @p line (excl. final null character).
 * @param status Location to write status code to.
 * @param unmapped_flag Location to write the unmapped flag to.
 * @param reverse_flag Location to write the reverse strand flag to.
 * @param cigar_start Location to write location of the beginning of the CIGAR
 *                    field to.
 * @param cigar_len Location to write CIGAR length to.
 * @param seq_start Location to write location of the beginning of the sequence
 *                  field to.
 * @param seq_len Location to write sequence length to.
 * @param qual_start Location to write location of the beginning of the quality
 *                  field to.
 * @param qual_len Location to write quality length to.
 * @param md_start Location to write location of the beginning of the MD tag
 *                 to.
 * @param md_len Location to write MD tag length to.
 * @return One if successful, zero otherwise.
 */
unsigned char
parse_line(const char *const line, size_t len, int *status,
           unsigned char *const unmapped_flag,
           unsigned char *const reverse_flag,
           const char **cigar_start, size_t *cigar_len,
           const char **seq_start, size_t *seq_len,
           const char **qual_start, size_t *qual_len,
           const char **md_start, size_t *md_len);


/**
 * @brief Decode a quality character to the corresponding score.
 *
 * This function converts a (Phred+33/Sanger/Illumina 1.8+) encoded quality
 * score from its character representation into its integer value.
 * No checks are performed during the convertion.
 *
 * @param qual_char Character representing the ((Phred+33/Sanger/Illumina 1.8+)
 *                  encoded quality score to decode.
 * @return Integer value of the quality score decoded from @p qual_char.
 */
unsigned short int
qual_char2score(const char qual_char);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_SAM_H */
