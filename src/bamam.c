/*****************************************************************************
 * BAMam core library source file                                            *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam core library source file.
 *
 * This file contains the implementation of the BAMam core functions defined in
 * @f bamam.h.
 *
 * @file bamam.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-23 - 2019-04-25
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <bamam.h>

/*
 * Include BAMam error code definition to get the following error codes:
 * - `NO_ERROR` to return upon success.
 * - `UNKNOWN_PROCESSING_ERROR` to return when processing a line fails in an
 *   unexpected way (should never happen).
 * - `UNKNOWN_ANNOTATION_ERROR` to return when annotating a line fails in an
 *   unexpected way (should never happen).
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR` to return when processing an
 *   incompatible combination of quality string and sequence.
 * - `UNKNOWN_MD_TAG_PROCESSING_ERROR` to return when processing an MD tag
 *   fails in an unexpected way (should never happen).
 * - `UNKNOWN_SEQUENCE_PROCESSING_ERROR` to return when processing a sequence
 *   fails in an unexpected way (should never happen).
 * - `INCOMPATIBLE_CIGAR_AND_MD_ERROR` to return when processing an
 *   incompatible combination of CIGAR string and MD tag.
 * - `INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR` to return when processing an
 *   incompatible combination of CIGAR string and sequence.
 * - `INVALID_NUCLEOTIDE_ERROR` to return when processing an invalid
 *   nucleotide.
 */
#include <errors.h>

/*
 * Include BAMam SAM parsing library to get the following functions:
 * - `is_header_line` to check if a line is a SAM header line.
 * - `parse_line` to parse a single non-header SAM line.
 * - `qual_char2score` to decode a quality character to the corresponding
 *   score.
 */
#include <sam.h>

/*
 * Include BAMam CIGAR parsing library to get the following functions:
 * - `get_next_cigar_char` to get the next CIGAR string character.
 * - `is_deletion_char` to check if a character is a deletion CIGAR character.
 * - `is_soft_clipping_char` to check if a character is a soft-clipping CIGAR
 *   character.
 * - `is_skipping_char` to check if a character is a skipping CIGAR character.
 * - `is_insertion_char` to check if a character is an insertion CIGAR character.
 */
#include <cigar.h>

/*
 * Include BAMam MD tag parsing library to get the following function:
 * - `parse_md_entry` to parse a single MD tag entry.
 */
#include <md.h>

/*
 * Include BAMam nucleotide handling library to get the following functions:
 * - `complement` to get the complement of a nucleotide character.
 * - `is_valid_nucleotide_char` to check if a character is a valid nucleotide
 *   character.
 */
#include <nucleotides.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify an invariant at runtime.
 */
#include <assert.h>

/*
 * Include standard input/output library to get the following functions:
 * - `putchar` to write single characters to STDOUT.
 * - `printf` to write output to STDOUT.
 * - `fprintf` to write output to STDERR.
 * - `getline` to read from STDIN line by line.
 */
#include <stdio.h>

/*
 * Include C standard library to get the following function:
 * - `free` to free memory allocated by `getline`.
 */
#include <stdlib.h>

/*
 * Include BAMam message printing library to get the following functions:
 * - `print_incompatible_sequence_and_quality_error` to print the
 *   incompatible-sequence-and-quality error message to STDERR.
 * - `print_incompatible_cigar_and_sequence_error` to print the
 *   incompatible-CIGAR-and-sequence error message to STDERR.
 * - `print_incompatible_cigar_and_md_error` to print the
 *   incompatible-CIGAR-and-MD error message to STDERR.
 */
#include <messages.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Print string prefix.
 *
 * This function prints the given length prefix of a given string to STDOUT.
 * It does not check for null-termination of the given string. Therefore, the
 * behaviour is undefined when passing a prefix length greater than the actual
 * string length.
 * This enables printing an arbitrary substring (by printing a prefix starting
 * in the middle) without the need to modify the string (e.g. to (temporarily)
 * introduce a null-character) or copy it (e.g. to extract the substring and
 * append it by a null-character), but instead directly print from read-only
 * memory.
 *
 * @param string String to print prefix of.
 * @param prefix_length Number of characters to print.
 */
void
print_prefix(const char *string, size_t prefix_length)
{
  /*
   * Count down the number of characters to print while printing one character
   * at a time, starting at the beginning and moving forward.
   * Note: Since parameters are passed by value, `string` can safely be moved
   * without causing any memory leak.
   * Also, `printf` could be used instead of `putchar` (with "%c" as format
   * string), but that would introduce unnecessary overhead.
   */
  while(prefix_length--)
  {
    putchar(*(string++));
  }
}


/**
 * @brief Get sequence nucleotide.
 *
 * This function returns the current sequence nucleotide based on the beginning
 * of the sequence (suffix), the (remaining) length of the sequence (suffix)
 * from that position on, and the current CIGAR string character.
 * If there is a nucleotide left in the sequence and the given CIGAR string
 * character does not indicate skipping or a deletion, not only is the
 * nucleotide returned, but also are the corresponding location of the
 * beginning of the remaining sequence suffix, and the remaining sequence
 * suffix length written to the locations pointed at by @p cursor, and @p len,
 * respectively, as well as the no-error code to the location pointed at by @p
 * status to indicate success. If the given CIGAR string character indicates
 * skipping or a deletion, a spacer character is returned instead.
 * If the sequence is incompatible with the corresponding CIGAR string
 * on the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status to indicate the invalidity
 * of the line, the null pointer is written to the location pointed to by @p
 * cursor and zero to the location pointed to by @p len, respectively, and the
 * null character is returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location to read/write the location of the beginning of the
 *               (remaining) sequence from/to.
 * @param len Location to read/write length of the sequence (suffix) pointed to
 *            by the pointer pointed to by @p cursor from/to.
 * @param cigar Current CIGAR string character.
 * @param status Location to write the (non-)error code to.
 * @return The next sequence nucleotide, if any, a spacer character otherwise.
 */
char
get_seq_nuc(const char const **cursor, size_t *len, const char cigar,
            int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_SEQUENCE_PROCESSING_ERROR;

  /*
   * Define spacer character to improve code readability.
   */
  const char SPACER_CHAR = '.';

  /*
   * Define error character to improve code readability.
   */
  const char ERROR_CHAR = '\0';


  /**********
   * spacer *
   **********/

  /*
   * If current CIGAR character indicates skipping or a deletion, return spacer
   * instead.
   */
  if(is_skipping_char(cigar) || is_deletion_char(cigar))
  {
    return SPACER_CHAR;
  }


  /************
   * conflict *
   ************/

  /*
   * This part of the code is only reached, if another (non-spacer) sequence
   * nucleotide is required to keep the sequence aligned to the corresponding
   * CIGAR string.
   */

  /*
   * If no nucleotide is left in the sequence, exit early reporting error.
   */
  if(!(*len))
  {
    *status = INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR;
    *cursor = NULL;
    print_incompatible_cigar_and_sequence_error();
    return ERROR_CHAR;
  }


  /**************
   * nucleotide *
   **************/

  /*
   * This part of the code is only reached when there is at least one
   * nucleotide left in the sequence.
   */

  /*
   * Return current nucleotide and move cursor to the next.
   */
  --(*len);
  return *((*cursor)++);
}


/**
 * @brief Get MD tag character.
 *
 * This function returns the current MD tag character based on the beginning of
 * the next entry within the MD tag, the (remaining) length of the MD tag
 * (suffix) from that position on, the numbers of (remaining) matches and
 * nucleotides in the current MD tag entry, and the current CIGAR string
 * character.
 * If there is at least one MD tag character left in the given MD tag (suffix)
 * and the given CIGAR string character is neither a skipping, nor a
 * soft-clipping or insertion character, not only is the next
 * mismatched/deleted nucleotide or a match character returned, but also are
 * the corresponding remaining MD tag suffix length and the match and deletion
 * counts written to the locations pointed at by @p len, @p matches, and @p
 * deletions, respectively, as well as the no-error code to the location
 * pointed at by @p status to indicate success. If the given CIGAR string
 * character indicates skipping, soft-clipping, or and insertion, a spacer
 * character is returned instead.
 * If the MD tag is invalid or incompatible with the corresponding CIGAR string
 * on the other hand, an error message is printed to STDERR, an error code is
 * written to the location pointed to by @p status to indicate the invalidity
 * of the MD tag entry or its incompatibility with the CIGAR string, the null
 * pointer is written to the location pointed to by @p cursor and zero to the
 * locations pointed to by @p matches, @p deletions, and @p len, respectively,
 * and the null character is returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the location of the beginning (i.e. first digit)
 *               of the next MD tag entry to parse.
 * @param len Location to read/write length of the MD tag (suffix) pointed to
 *            by @p cursor from/to.
 * @param matches Location to read/write the (remaining) match count of the
 *                current MD tag entry from/to.
 * @param deletions Location to read/write the (remaining) deletion count of
 *                the current MD tag entry from/to.
 * @param cigar Current CIGAR string character.
 * @param status Location to write the (non-)error code to.
 * @return The next MD tag character (mismatched/deleted nucleotide or a match
 *         character, or a spacer character if MD tag (suffix) is empty) if the
 *         processed part of the MD tag (suffix) is valid, the null pointer
 *         otherwise.
 */
char
get_md_char(const char const **cursor, size_t *len, size_t *matches,
            size_t *nucleotides, const char cigar, int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_MD_TAG_PROCESSING_ERROR;

  /*
   * Define spacer character to improve code readability.
   */
  const char SPACER_CHAR = '.';

  /*
   * Define error character to improve code readability.
   */
  const char ERROR_CHAR = '\0';

  /*
   * Define match character to improve code readability.
   */
  const char MATCH_CHAR = '=';

  /**
   * Initialize MD character to return.
   */
  char md_char = ERROR_CHAR;

  /*
   * Initialize MD deletion count with zero (no deletion).
   */
  size_t deletions = 0;


  /**********
   * spacer *
   **********/

  /*
   * If current CIGAR character indicates skipping, soft-clipping, or an
   * insertion, return a spacer instead.
   * Note: Due to C's short-circuit evaluation, performance of this statement
   * improves if the comparisons are ordered from most to least common.
   */
  if(   is_skipping_char(cigar)
     || is_soft_clipping_char(cigar)
     || is_insertion_char(cigar))
  {
    *status = NO_ERROR;
    return SPACER_CHAR;
  }


  /************
   * conflict *
   ************/

  /*
   * This part of the code is only reached, if another (non-spacer) MD tag
   * character is required to keep the MD tag aligned to the corresponding
   * CIGAR string.
   */

  /*
   * If no MD tag character is left, exit early reporting error.
   */
  if(!(*matches || *nucleotides || *len))
  {
    *status = INCOMPATIBLE_CIGAR_AND_MD_ERROR;
    *cursor = NULL;
    print_incompatible_cigar_and_md_error();
    return ERROR_CHAR;
  }


  /***********
   * parsing *
   ***********/

  /*
   * If the current MD tag entry was processed completely, try to parse the
   * next one and error out if unsuccessful.
   */
  if(!(*matches || *nucleotides))
  {
    *cursor = parse_md_entry(*cursor, len, matches, &deletions, status);
    if(!(*cursor))
    {
      *len = 0;
      *matches = 0;
      *nucleotides = 0;
      assert(*status != NO_ERROR);
      return ERROR_CHAR;
    }

    /**
     * @todo: This block needs some explanatory comment.
     */
    if(!deletions)
    {
      *nucleotides = (*len > 1);
    }
    else
    {
      *nucleotides = deletions;
    }
  }


  /**************
   * processing *
   **************/

  /*
   * If the current MD tag entry has matches left, set the return value to a
   * match character and subtract the processed match from the MD tag entry.
   * Otherwise, set the return value to the (next) deleted/mismatched character
   * of the current MD tag entry, if any is left.
   */
  if(*matches)
  {
    md_char = MATCH_CHAR;
    --(*matches);
  }
  else
  {
    if(*nucleotides)
    {
      md_char = *(*cursor - (--(*nucleotides)));
    }
  }

  /*
   * If the current MD tag entry was processed completely, move MD tag
   * cursor to the next one, if any is left.
   */
  if(!(*matches || *nucleotides) && *len)
  {
    --(*len);
    (*cursor)++;
  }


  /****************
   * finalization *
   ****************/

  /*
   * Return the current MD tag character indicating success.
   */
  *status = NO_ERROR;
  return md_char;
}


/**
 * @brief Annotate single (valid) non-header line.
 *
 * This function produces a single output line of the BAMam core algorithm
 * based on a single valid non-header input line, its CIGAR, sequence and
 * quality fields and MD tag, as well as the corresponding lengths. The
 * behaviour is undefined when passing a line that does not satisfy
 * `parse_line`.
 * Upon success, the generated line is directly output to STDOUT and zero is
 * returned. Upon error, the corresponding error message is printed to STDERR
 * and the corresponding error code is returned.
 * See @f errors.h for the error code definition.
 *
 * @param line_start Beginning of a valid non-header input line.
 * @param line_len Length of the input line (without final newline).
 * @param is_umapped Boolean flag indicating whether or not the alignment
 *                   corresponds to am unmapped read.
 * @param is_reverse Boolean flag indicating whether or not the alignment
 *                   corresponds to a reverse-mapped read.
 * @param cigar_start Beginning of the CIGAR field within the input line.
 * @param cigar_len Length of the CIGAR field.
 * @param seq_start Beginning of the sequence field within the input line.
 * @param seq_len Length of the sequence field.
 * @param qual_start Beginning of the quality field within the input line.
 * @param qual_len Length of the quality field.
 * @param md_start Beginning of the MD tag within the input line.
 * @param md_len Length of the MD tag.
 * @return Zero if successful, an error code otherwise.
 */
int
annotate_line(const char *const line_start, const size_t line_len,
              const unsigned char is_unmapped,
              const unsigned char is_reverse,
              const char *const cigar_start, const size_t cigar_len,
              const char *const seq_start, const size_t seq_len,
              const char *const qual_start, const size_t qual_len,
              const char *const md_start, const size_t md_len)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  int status = UNKNOWN_ANNOTATION_ERROR;

  /*
   * Define boolean values to improve code readability.
   */
  const unsigned char TRUE = 1;
  const unsigned char FALSE = 0;

  /*
   * Define tag prefix string (including SAM field separator) to improve code
   * readability.
   */
  const char *const TAG_PREFIX = "\tMM:Z:";

  /*
   * Define tag entry separator character to improve code readability.
   */
  const char SEPARATOR = ';';

  /*
   * Define deletion character to improve code readability.
   */
  const char DELETION_CHAR = '-';

  /*
   * Define newline to improve code readability.
   */
  const char NEWLINE = '\n';

  /*
   * Initialize CIGAR count with zero (empty sum).
   */
  size_t cigar_cnt = 0;

  /*
   * Initialize unparsed CIGAR suffix length to CIGAR length (nothing parsed).
   */
  size_t cigar_remaining = cigar_len;

  /*
   * Initialize CIGAR parsing cursor to CIGAR string start (nothing parsed).
   */
  const char *cigar_cursor = cigar_start;

  /*
   * Initialize unparsed sequence suffix length to sequence length (nothing parsed).
   */
  size_t seq_remaining = seq_len;

  /*
   * Initialize sequence parsing cursor to sequence string start (nothing parsed).
   */
  const char *seq_cursor = seq_start;

  /*
   * Initialize MD match count with zero (empty sum).
   */
  size_t md_match_cnt = 0;

  /*
   * Initialize MD nucleotide count with zero (no mismatch/deletion).
   */
  size_t md_nuc_cnt = 0;

  /*
   * Initialize unparsed MD tag suffix length to MD tag length (nothing parsed).
   */
  size_t md_remaining = md_len;

  /*
   * Initialize MD tag parsing cursor to MD tag start (nothing parsed).
   */
  const char *md_cursor = md_start;

  /*
   * Initialize read position with zero for forward-mapped reads or one plus
   * the read length for reverse-mapped reads (nothing parsed).
   */
  size_t read_pos = is_reverse * (seq_len + 1) ;

  /*
   * Fix read position step to one for forward-mapped reads or minus one for
   * reverse-mapped reads.
   */
  const short int read_pos_step = 1 - 2 * is_reverse;

  /*
   * Initialize reference position offset with zero (nothing parsed).
   */
  size_t reference_offset = 0;

  /*
   * Initialize first tag entry boolean flag with true (nothing parsed).
   */
  size_t is_first_tag_entry = TRUE;


  /********************
   * input validation *
   ********************/

  /*
   * Ensure that the quality string and the sequence are compatible (i.e. have
   * identical lengths) and exit early reporting error otherwise.
   */
  if(qual_len != seq_len)
  {
    status = INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR;
    print_incompatible_sequence_and_quality_error(seq_start, seq_len,
                                                  qual_start, qual_len,
                                                  line_start);
    return status;
  }


  /******************
   * unmapped reads *
   ******************/

  /*
   * Print the unannotated input line.
   */
  print_prefix(line_start, line_len);

  /*
   * Append final newline to unannotated input line and exit early reporting
   * success for alignments corresponding to an unmapped read (no new tag
   * required).
   */
  if(is_unmapped)
  {
    status = NO_ERROR;
    putchar(NEWLINE);
    return status;
  }


  /*************
   * tag setup *
   *************/

  /*
   * This part of the code is only reached if the current line corresponds to a
   * mapped read.
   */

  /*
   * Append the tag prefix to the unannotated input line.
   */
  printf(TAG_PREFIX);


  /***************
   * tag entries *
   ***************/

  /*
   * Iterate over the CIGAR string, and ensure it is as long or longer than the
   * sequence and the MD tag.
   * For each CIGAR character, print a single line with the CIGAR character and
   * the corresponding (unaligned) sequence nucleotide and MD tag character.
   */
  while(cigar_remaining || cigar_cnt || seq_remaining || md_match_cnt
        || md_nuc_cnt || md_remaining)

  {
    assert(cigar_remaining);

    /*
     * Get the location of the next CIGAR character and exit early printing the
     * offending line to STDERR upon error.
     */
    const char const *cigar_char =
      get_next_cigar_char(&cigar_cursor, &cigar_remaining, &cigar_cnt,
                          &status);
    if(!cigar_char)
    {
      fprintf(stderr, "%s", line_start);
      assert(status != NO_ERROR);
      return status;
    }

    /*
     * Get the corresponding sequence nucleotide and exit early printing the
     * offending line to STDERR upon error.
     */
    const char seq_nuc = get_seq_nuc(&seq_cursor, &seq_remaining, *cigar_char,
                                     &status);
    if(!seq_cursor)
    {
      fprintf(stderr, "%s", line_start);
      assert(status != NO_ERROR);
      return status;
    }

    /*
     * Get the corresponding quality score.
     * Set it to zero if the CIGAR string character indicates a deletion
     * (as the sequence cursor still refers to the upstream nucleotide).
     * Note: Since the validity of the sequence and quality fields has been
     * verified, the quality corresponding to any nucleotide is a fixed number
     * of characters downstream: The remaining nucleotides plus the field
     * separator plus the quality characters corresponding to the preceding
     * nucleotides. Thus, the total offset equals the sequence length (or by
     * equality the quality string length, as verified above) plus one (for the
     * field separator. However, since the sequence nucleotide was already
     * processed above, the sequence cursor already points to the following
     * sequence nucleotide. Therefore, to get from the sequence cursor to the
     * current quality character, the field separator is compensated by that
     * cursor disposition.
     */
    const unsigned short int qual_score =
      qual_char2score(*(seq_cursor + seq_len))
      * !is_deletion_char(*cigar_char);

    /*
     * Get the corresponding MD tag character and exit early printing the
     * offending line to STDERR upon error.
     */
    const char md_char =
      get_md_char(&md_cursor, &md_remaining, &md_match_cnt, &md_nuc_cnt,
                  *cigar_char, &status);
    if(!md_cursor)
    {
      fprintf(stderr, "%s", line_start);
      assert(status != NO_ERROR);
      return status;
    }


  /***************
   * coordinates *
   ***************/

    /*
     * Move read position unless the current CIGAR string character indicates
     * skipping or a deletion.
     * Note: Due to C's short-circuit evaluation, performance of this statement
     * improves if the comparisons are ordered from most to least common.
     */
    read_pos += read_pos_step * !(   is_skipping_char(*cigar_char)
                                  || is_deletion_char(*cigar_char));

    /*
     * Move reference position unless the current CIGAR string character
     * indicates soft-clipping or an insertion.
     * Note: Due to C's short-circuit evaluation, performance of this statement
     * improves if the comparisons are ordered from most to least common.
     */
    reference_offset += !(   is_soft_clipping_char(*cigar_char)
                          || is_insertion_char(*cigar_char));


  /**************
   * annotation *
   **************/

    /**
     * @todo Actually annotate the lines.
     */

    /*
     * Print tag entries for mismatches and deletions.
     * Note: For skipped and soft-clipped positions, the MD tag character is a
     * spacer character, for matches a match character. Thus, only for
     * mismatches and deletions it will be a valid nucleotide.
     */
    /**
     * @todo Factor-out into smaller function(s).
     */
    if(is_valid_nucleotide_char(md_char))
    {

      /*************
       * separator *
       *************/

      /*
       * Separate individual tag entries by a separator character but do not
       * prepend first tag entry with a separator.
       */
      if(is_first_tag_entry)
      {
        is_first_tag_entry = FALSE;
      }
      else
      {
        putchar(SEPARATOR);
      }


      /*****************************
       * reference position offset *
       *****************************/

      /*
       * Print reference position offset in the first column of the output.
       * Note: The subtraction shifts one-base coordinates to zero-base
       * coordinates truncating at zero. This avoids integer overflow without
       * the need to reserve half of the bits for negative values that are
       * never needed.
       */
      printf("%lu", (unsigned long) reference_offset - (reference_offset > 0));


      /************************
       * reference nucleotide *
       ************************/

      /*
       * Print reference nucleotide in the second column of the output.
       * For reverse-mapped reads, complement it first.
       * Note: Since the complementation is only attempted for valid
       * nucleotides, it must never fail (thus the assertion).
       */
      if(is_reverse)
      {
        const char md_char_comp = complement(md_char, &status);
        assert(status == NO_ERROR);
        putchar(md_char_comp);
      }
      else
      {
        putchar(md_char);
      }


      /*****************
       * quality score *
       *****************/

      /*
       * Print quality score in the third column of the tag entry.
       */
      printf("%hu", qual_score);


      /*******************
       * read nucleotide *
       *******************/

      /*
       * Print deletion character for deletions or read nucleotide for
       * mismatches in the fourth column of the output.
       * For reverse-mapped reads, complement the nucleotide first and exit
       * early reporting failure upon error.
       */
      if(is_deletion_char(*cigar_char))
      {
        putchar(DELETION_CHAR);
      }
      else
      {
        if(is_reverse)
        {
          const char seq_nuc_comp = complement(seq_nuc, &status);
          if(!seq_nuc_comp)
          {
            fprintf(stderr, "%s", line_start);
            assert(status != NO_ERROR);
            return status;
          }
          putchar(seq_nuc_comp);
        }
        else
        {
          putchar(seq_nuc);
        }
      }


      /*****************
       * read position *
       *****************/

      /*
       * Print read position in the fifth column of the output.
       */
      printf("%lu", (unsigned long) read_pos);
    }
  }

  /*
   * Move on to the next output line for the next input line.
   */
  putchar(NEWLINE);


  /****************
   * finalization *
   ****************/

  /*
   * This part of the code is only reached after successfully annotating a
   * valid non-header line.
   * Thus, the `UNKNOWN_ANNOTATION_ERROR` error code `status` was initialized
   * to should have been overwritten with the no-error success code.
   */
  assert(status == NO_ERROR);

  /*
   * Report success.
   * Note: This is equivalent to `return status;` as asserted above.
   */
  return NO_ERROR;
}


/**
 * @brief Process single input line.
 *
 * This function produces a single output line of the BAMam core algorithm
 * based on a single input line.
 * Upon success, the generated line is directly output to STDOUT and zero is
 * returned. Upon error, the corresponding error message is printed to STDERR
 * and the corresponding error code is returned.
 * See @f errors.h for the error code definition.
 *
 * @param line Input line (incl. final newline, null-terminated) to process.
 * @param len Length of @p line (excl. final null character).
 * @return Zero if successful, an error code otherwise.
 */
int
process_line(const char *const line, const size_t len)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  int error = UNKNOWN_PROCESSING_ERROR;

  /*
   * Define boolean false to improve code readability.
   */
  const unsigned char FALSE = 0;

  /*
   * Initialize unmapped flag.
   */
  unsigned char unmapped = FALSE;

  /*
   * Initialize reverse strand flag.
   */
  unsigned char reverse_strand = FALSE;

  /*
   * Initialize CIGAR field start pointer and length.
   */
  const char *cigar_start = NULL;
  size_t cigar_len = 0;

  /*
   * Initialize sequence field start pointer and length.
   */
  const char *seq_start = NULL;
  size_t seq_len = 0;

  /*
   * Initialize quality field start pointer and length.
   */
  const char *qual_start = NULL;
  size_t qual_len = 0;

  /*
   * Initialize MD tag start pointer and length.
   */
  const char *md_start = NULL;
  size_t md_len = 0;


  /****************
   * header lines *
   ****************/

  /*
   * Copy header lines from input to output without changing them and exit
   * early reporting success.
   */
  if(is_header_line(line))
  {
    printf("%s", line);
    return NO_ERROR;
  }


  /***************
   * parse lines *
   ***************/

  /*
   * This part of the code is only reached when processing a non-header line.
   */

  /*
   * Parse line capturing the overall status (valid/invalid) and the specific
   * error code (if any).
   */
  const unsigned char line_valid =
    parse_line(line, len, &error,
               &unmapped,
               &reverse_strand,
               &cigar_start, &cigar_len,
               &seq_start, &seq_len,
               &qual_start, &qual_len,
               &md_start, &md_len);


  /*****************
   * invalid lines *
   *****************/

  /*
   * Exit early passing up the error code when encountering an invalid line.
   */
  if(!line_valid)
  {
    return error;
  }


  /*******************
   * alignment lines *
   *******************/

  /*
   * This part of the code is only reached when processing a valid non-header
   * line.
   * Thus, `parse_line` should have overwritten the unknown-processing-error
   * code `error` was initialized to with the no-error success code.
   */
  assert(error == NO_ERROR);

  /*
   * Annotate valid non-header lines and pass up the corresponding status code.
   * Note: The line length has to be decrease by one character to exclude the
   * final newline.
   */
  return annotate_line(line, len - 1,
                       unmapped,
                       reverse_strand,
                       cigar_start, cigar_len,
                       seq_start, seq_len,
                       qual_start, qual_len,
                       md_start, md_len);
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Run BAMam core algorithm for each line input from STDIN and output annotated
 * line to STDOUT.
 */
int
bamam()
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the status with the success code to make BAMam 'successfully'
   * process empty input into empty output.
   */
  int status = NO_ERROR;

  /*
   * Initialize line reading buffer.
   * Note: While `readline` will allocate memory via `malloc` and `realloc` as
   * needed, it *won't* release the memory it allocated via `free`.
   */
  char *line_buffer = NULL;

  /*
   * Initialize line reading buffer size.
   * Note: This *must* reflect the actual buffer size allocated at the location
   * stored at `line_buffer`. If the buffer is resized by `getline`, this is
   * taken care of.
   * Since `line_buffer` is initialized `NULL` relying on `readline` to
   * actually allocate the initial buffer memory, the initial buffer size is
   * set to zero.
   */
  size_t line_buffer_size = 0;

  /*
   * Initialize buffered line length.
   * Note: This might be smaller than `line_buffer_size` as the current line
   * might not fill the buffer completely. However, it must not be larger as
   * the buffer must fit the line. This can be ensured by assigning
   * `readline`'s return value to `line_length`. Therefor, unlike
   * `line_buffer`, `line_length` must be assigned a signed type as `readline`
   * returns minus one when attempting to read beyond the last line.
   * Since no  data has been read successfully upon initialization,
   * `line_length` is initialized with a negative value. To enable to
   * differentiate `getline` failing to read (another) line and no attempt of
   * reading (any) line being made (yet), however, the initial `line_length` is
   * set to minus two rather than minus one.
   */
  ssize_t line_length = -2;


  /*****************
   * process lines *
   *****************/

  /*
   * Read STDIN line-by-line and process each line individually.
   * Note: As each line can be processed independently of all other lines,
   * BAMam could be sped up by parallelizing this part of the code. However,
   * care would need to be taken to ensure the output is printed in order and
   * the status is handled properly.
   */
  while((line_length = getline(&line_buffer, &line_buffer_size, stdin)) != -1)
  {
    /*
     * Process current line and catch the status.
     */
    status = process_line(line_buffer, line_length);

    /*
     * Note: As the same buffer is re-used without releasing memory via `free`
     * in-between `readline`s, the buffer can only grow. I.e. at any the buffer
     * time will be big enough to fit the longest line read so far. This could
     * be changed by calling `free(line); line_buffer = NULL;` here within the
     * loop instead of outside the loop. However, this would result in a lot of
     * memory (re-)allocation overhead increasing the runtime to reduce the
     * average memory usage. Given that the peak memory usage would remain
     * unchanged (as the longest buffer still has to fit the longest line when
     * processing it) and SAM lines are relatively 'small' in comparison the
     * memory typically available to the expected users (bioinformaticians),
     * runtime is prioritized over memory usage here. Additionally, most SAM
     * files have lines of very similar length (within the same file, not
     * necessarily across files) which would reduce the actual memory gain
     * (which scales linear with the difference between the longest line length
     * and the average line length) while keeping the performance penalty
     * (which scales linear with the number of lines) constant.
     */

    /*
     * Stop processing any further input lines upon error.
     */
    if(status != NO_ERROR)
    {
      break;
    }
  }


  /****************
   * finalization *
   ****************/

  /*
   * Release memory (potentially) allocated by `getline` to avoid memory leak.
   * Note: It is possible that `getline` errors out returning `-1` without ever
   * allocating any memory. In this case, `line_buffer` would still be a null
   * pointer at this point. While there are C implementations failing when
   * attempting to run `free(NULL)`, standard C defines this a no-op. Thus, no
   * `if(line_buffer)` check is performed at this point.
   */
  free(line_buffer);

  /*
   * Unset dangling pointer.
   * Note: Even though the memory that used to be allocated at the position
   * pointed to by `line_buffer` was released via `free`, `line_pointer` still
   * points to the memory address where that memory block used to reside. This
   * could lead to undefined behaviour when (accidentally) writing to memory
   * via that 'dangling' pointer. By (re-)setting the pointer to `NULL`, any
   * such writing attempt would result in a (controlled) run-time error.
   */
  line_buffer = NULL;

  /*
   * Pass up the status of processing the last processed line.
   * Note: In case of an error during processing of any line, no further line
   * is read making the line the error occurred on the last processed line.
   * Thus, `bamam` will return the error code corresponding to the failing
   * line. On the other hand, if all line are processed without errors, the
   * very last input line will be processed report success (just as all the
   * other ones before it). Therefor, error or not, `bamam` will always return
   * the correct value.
   */
  return status;
}
