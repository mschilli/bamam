/*****************************************************************************
 * BAMam core library header file                                            *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam core library header file.
 *
 * This is the header file for BAMam's core library.
 *
 * @file bamam.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-23
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_BAMAM_H
#define BAMAM_BAMAM_H


/*************
 * functions *
 *************/

/**
 * @brief Check command line arguments.
 *
 * This function runs the BAMam core algorithm.
 * See @f errors.h for the error code definition.
 *
 * @return `0` on success, an error code otherwise.
 */
int
bamam();


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_BAMAM_H */
