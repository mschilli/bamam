/*****************************************************************************
 * BAMam nucleotide handling library header file                             *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam nucleotide handling library header file.
 *
 * This is the header file for BAMam's nucleotide handling library.
 *
 * @file nucleotides.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-04-10
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_NUCLEOTIDES_H
#define BAMAM_NUCLEOTIDES_H


/*************
 * functions *
 *************/

/**
 * @brief Check if character is valid nucleotide character.
 *
 * This function checks for a single character, if it is a valid nucleotide
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is a valid nucleotide character, zero otherwise.
 */
unsigned char
is_valid_nucleotide_char(const char character);


/**
 * @brief Complement nucleotide character.
 *
 * This function returns the complement of a given nucleotide character.
 * If the given character is a valid nucleotide, not only its complement
 * returned, but also is the no-error code to the location pointed at by
 * @p status to indicate success. If this is not the case on the other hand, an
 * error message is printed to STDERR, an error code is written to the location
 * pointed to by @p status to indicate the invalidity of the nucleotide, and
 * the null character is returned.
 * See @f errors.h for the error code definition.
 *
 * @param nucleotide Nucleotide character to return the complement for.
 * @param status Location to write the (non-)error code to.
 * @return The complement nucleotide character of @p nucleotide, if it is a
 *         valid nucleotide character, the null character otherwise.
 */
char
complement(const char nucleotide, int *status);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_NUCLEOTIDES_H */
