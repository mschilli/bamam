/*****************************************************************************
 * BAMam message printing library source file                                *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam message printing library source file.
 *
 * This file contains the implementation of the BAMam message printing
 * functions defined in @f messages.h.
 *
 * @file messages.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-11 - 2019-04-12
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 * This header also defines the following string constants used here:
 * - `USAGE_ERROR_MESSAGE`
 * - `USAGE_MESSAGE_PRE`
 * - `USAGE_MESSAGE_POST`
 * - `HELP_HINT_MESSAGE_PRE`
 * - `HELP_HINT_MESSAGE_POST`
 * - `TOO_MANY_ARGUMENTS_ERROR_MESSAGE_PRE`
 * - `TOO_MANY_ARGUMENTS_ERROR_MESSAGE_MID`
 * - `TOO_MANY_ARGUMENTS_ERROR_MESSAGE_POST`
 * - `INVALID_FLAG_ERROR_MESSAGE_PRE`
 * - `INVALID_FLAG_ERROR_MESSAGE_POST`
 * - `HELP_MESSAGE_PRE`
 * - `HELP_MESSAGE_POST`
 * - `INVALID_LINE_ERROR_MESSAGE`
 * - `INVALID_CIGAR_ERROR_MESSAGE`
 * - `TOO_FEW_FIELDS_ERROR_MESSAGE`
 * - `NO_SAM_FLAG_FIELD_ERROR_MESSAGE_PRE`
 * - `SAM_FLAG_FIELD_LAST_ERROR_MESSAGE_PRE`
 * - `INVALID_SAM_FLAG_ERROR_MESSAGE_PRE`
 * - `INVALID_SAM_FLAG_ERROR_MESSAGE_MID`
 * - `NO_CIGAR_FIELD_ERROR_MESSAGE_PRE`
 * - `CIGAR_FIELD_LAST_ERROR_MESSAGE_PRE`
 * - `NO_SEQ_FIELD_ERROR_MESSAGE_PRE`
 * - `SEQ_FIELD_LAST_ERROR_MESSAGE_PRE`
 * - `NO_TAG_ERROR_MESSAGE_PRE`
 * - `NO_MD_TAG_ERROR_MESSAGE_PRE`
 * - `MISSING_CIGAR_COUNT_ERROR_MESSAGE_PRE`
 * - `MISSING_CIGAR_COUNT_ERROR_MESSAGE_POST`
 * - `MISSING_CIGAR_CHAR_ERROR_MESSAGE_PRE`
 * - `MISSING_CIGAR_CHAR_ERROR_MESSAGE_POST`
 * - `INVALID_CIGAR_CHAR_ERROR_MESSAGE_PRE`
 * - `INVALID_CIGAR_CHAR_ERROR_MESSAGE_POST`
 * - `INVALID_MD_TAG_ERROR_MESSAGE`
 * - `MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_PRE`
 * - `MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_POST`
 * - `INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_PRE`
 * - `INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_POST`
 * - `MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE`
 * - `MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST`
 * - `INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE`
 * - `INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_PRE`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID1`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID2`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID3`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID4`
 * - `INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_POST`
 * - `INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR_MESSAGE`
 * - `INCOMPATIBLE_CIGAR_AND_MD_ERROR_MESSAGE`
 * - `INVALID_NUCLEOTIDE_ERROR_MESSAGE_PRE`
 * - `INVALID_NUCLEOTIDE_ERROR_MESSAGE_POST`
 * See the header file for the documentation of these constants.
 */
#include <messages.h>

/*
 * Include standard input/output library to get the following functions:
 * - `fprintf` to print to STDERR.
 * - `printf` to print to STDOUT.
 */
#include <stdio.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify invariant at runtime.
 */
#include <assert.h>

/*
 * Include GNU Autotools configuration header to get the following macro:
 * - `VERSION` expands to the version number defined through the GNU Autotools.
 */
#include <config.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Print usage error message.
 *
 * This function prints the usage error message to STDERR.
 *
 * @param cmd Command used to call BAMam.
 */
void
print_usage_error_message()
{
  /*
   * Print the usage error message to STDERR.
   */
  fprintf(stderr, "%s", USAGE_ERROR_MESSAGE);
}


/**
 * @brief Print usage message.
 *
 * This function prints the usage message to the given output stream.
 *
 * @param cmd Command used to call BAMam.
 * @param output_stream Output stream (`stdout` or `stderr`) to print to.
 */
void
print_usage_message(const char const *cmd, FILE *output_stream)
{
  /*
   * This function is supposed to write the usage message to either STDOUT or
   * STDERR. To check this at compile time, a dedicated type would be needed
   * introducing a lot of boilerplate code. Instead, this is checked at runtime
   * relying on `make check` to cover the code well enough to catch any invalid
   * call of this function.
   */
  assert(output_stream == stdout || output_stream == stderr);

  /*
   * Print the individual parts of the usage message to the output stream.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  fprintf(output_stream, "%s%s%s", USAGE_MESSAGE_PRE, cmd, USAGE_MESSAGE_POST);
}


/**
 * @brief Print help hint message.
 *
 * This function prints the help hint message to STDERR.
 *
 * @param cmd Command used to call BAMam.
 */
void
print_help_hint_message(const char const *cmd)
{
  /*
   * Print the individual parts of the help hint message to STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  fprintf(stderr, "%s%s%s",
          HELP_HINT_MESSAGE_PRE, cmd, HELP_HINT_MESSAGE_POST);
}


/**
 * @brief Print invalid-line error message.
 *
 * This function prints the invalid-line error message to STDERR.
 */
void
print_invalid_line_error_message()
{
  fprintf(stderr, "%s", INVALID_LINE_ERROR_MESSAGE);
}


/**
 * @brief Print invalid-CIGAR error message.
 *
 * This function prints the invalid-CIGAR error message to STDERR.
 */
void
print_invalid_cigar_error_message()
{
  fprintf(stderr, "%s", INVALID_CIGAR_ERROR_MESSAGE);
}


/**
 * @brief Print error prefix.
 *
 * This function prints the given length prefix of a given string to STDERR.
 * It does not check for null-termination of the given string. Therefore, the
 * behaviour is undefined when passing a prefix length greater than the actual
 * string length.
 * This enables printing an arbitrary substring (by printing a prefix starting
 * in the middle) without the need to modify the string (e.g. to (temporarily)
 * introduce a null-character) or copy it (e.g. to extract the substring and
 * append it by a null-character), but instead directly print from read-only
 * memory.
 *
 * @param string String to print prefix of.
 * @param prefix_length Number of characters to print.
 */
void
print_error_prefix(const char *string, size_t prefix_length)
{
  /*
   * Count down the number of characters to print while printing one character
   * at a time, starting at the beginning and moving forward.
   * Note: Since parameters are passed by value, `string` can safely be moved
   * without causing any memory leak.
   * Also, `fprintf` could be used instead of `fputc` (with "%c" as format
   * string), but that would introduce unnecessary overhead.
   */
  while(prefix_length--)
  {
    fputc(*(string++), stderr);
  }
}


/**
 * @brief Print invalid-MD-tag error message.
 *
 * This function prints the invalid-MD-tag error message to STDERR.
 */
void
print_invalid_md_tag_error_message()
{
  fprintf(stderr, "%s", INVALID_MD_TAG_ERROR_MESSAGE);
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Verify invariants and print too-many-arguments error message to STDERR.
 */
void
print_too_many_arguments_error(const char const *cmd,
                               const unsigned int argc,
                               const unsigned int max)
{
  /*
   * This functions prints an error message stating that too many arguments
   * were passed to BAMam. Thus, it should only be called if more argument were
   * given (`argc`) than maximally supported (`max`).
   */
  assert(argc > max);

  /*
   * Both, `argc` and `max` are defined to include the command used to invoke
   * BAMam itself (`argv[0]`) as a counted argument. This rules out zero as a
   * valid value.
   * As `argc` was already verified to be larger than `max`, it is sufficient
   * to check the latter one for that lower bound.
   */
  assert(max > 1);

  /*
   * Print the individual parts of the too-many-arguments error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   * Subtract the command used to invoke BAMam itself (`argv[0]`) from the
   * argument counts.
   */
  print_usage_error_message();
  fprintf(stderr, "%s%u%s%u%s",
          TOO_MANY_ARGUMENTS_ERROR_MESSAGE_PRE, argc - 1,
          TOO_MANY_ARGUMENTS_ERROR_MESSAGE_MID, max - 1,
          TOO_MANY_ARGUMENTS_ERROR_MESSAGE_POST);
  print_usage_message(cmd, stderr);
  print_help_hint_message(cmd);
}


/*
 * Print the invalid-flag error message to STDERR.
 */
void
print_invalid_flag_error(const char const *cmd, const char const *flag)
{
  /*
   * Print the individual parts of the too-many-arguments error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_usage_error_message();
  fprintf(stderr, "%s%s%s",
          INVALID_FLAG_ERROR_MESSAGE_PRE, flag,
          INVALID_FLAG_ERROR_MESSAGE_POST);
  print_usage_message(cmd, stderr);
  print_help_hint_message(cmd);
}


/*
 * Print the help message to STDOUT.
 */
void
print_help(const char const *cmd)
{
  /*
   * Print the individual parts of the help message to STDOUT.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  printf("%s", HELP_MESSAGE_PRE);
  print_usage_message(cmd, stdout);
  printf("%s", HELP_MESSAGE_POST);
}


/*
 * Print the version number defined through the GNU Autotools to STDOUT.
 */
void
print_version()
{
  printf("%s\n", VERSION);
}


/*
 * Print the too-few-fields invalid-line error message to STDERR.
 */
void
print_too_few_fields_error()
{
  print_invalid_line_error_message();
  fprintf(stderr, "%s", TOO_FEW_FIELDS_ERROR_MESSAGE);
}


/*
 * Print the no-SAM-flag-field error message to STDERR.
 */
void
print_no_sam_flag_field_error(const char const *line)
{
  fprintf(stderr, "%s%s", NO_SAM_FLAG_FIELD_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the SAM-flag-field-last error message to STDERR.
 */
void
print_sam_flag_field_last_error(const char const *line)
{
  fprintf(stderr, "%s%s", SAM_FLAG_FIELD_LAST_ERROR_MESSAGE_PRE, line);
}


/*
 *
 * Print the invalid-SAM-flag error message to STDERR.
 */
void
print_invalid_sam_flag_error(const char const *sam_flag_start,
                             const size_t sam_flag_len,
                             const char const *line)
{
  /*
   * Print the individual parts of the invalid-SAM-flag error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_line_error_message();
  fprintf(stderr, "%s", INVALID_SAM_FLAG_ERROR_MESSAGE_PRE);
  print_error_prefix(sam_flag_start, sam_flag_len);
  fprintf(stderr, "%s%s", INVALID_SAM_FLAG_ERROR_MESSAGE_MID, line);
}



/*
 * Print the no-CIGAR-field error message to STDERR.
 */
void
print_no_cigar_field_error(const char const *line)
{
  fprintf(stderr, "%s%s", NO_CIGAR_FIELD_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the CIGAR-field-last error message to STDERR.
 */
void
print_cigar_field_last_error(const char const *line)
{
  fprintf(stderr, "%s%s", CIGAR_FIELD_LAST_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the no-sequence-field error message to STDERR.
 */
void
print_no_seq_field_error(const char const *line)
{
  fprintf(stderr, "%s%s", NO_SEQ_FIELD_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the sequence-field-last error message to STDERR.
 */
void
print_seq_field_last_error(const char const *line)
{
  fprintf(stderr, "%s%s", SEQ_FIELD_LAST_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the no-tag error message to STDERR.
 */
void
print_no_tag_error(const char const *line)
{
  fprintf(stderr, "%s%s", NO_TAG_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the no-MD-tag error message to STDERR.
 */
void
print_no_md_tag_error(const char const *line)
{
  fprintf(stderr, "%s%s", NO_MD_TAG_ERROR_MESSAGE_PRE, line);
}


/*
 * Print the missing-CIGAR-count error message to STDERR.
 */
void
print_missing_cigar_count_error(const char const *cigar_suffix_start,
                                const size_t cigar_suffix_len)
{
  /*
   * Print the individual parts of the missing-CIGAR-count error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_cigar_error_message();
  fprintf(stderr, "%s", MISSING_CIGAR_COUNT_ERROR_MESSAGE_PRE);
  print_error_prefix(cigar_suffix_start, cigar_suffix_len);
  fprintf(stderr, "%s", MISSING_CIGAR_COUNT_ERROR_MESSAGE_POST);
}


/*
 * Print the missing-CIGAR-character error message to STDERR.
 */
void
print_missing_cigar_char_error(const size_t cigar_count)
{
  /*
   * Print the individual parts of the missing-CIGAR-character error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_cigar_error_message();
  fprintf(stderr, "%s%lu%s",
          MISSING_CIGAR_CHAR_ERROR_MESSAGE_PRE, (unsigned long) cigar_count,
          MISSING_CIGAR_CHAR_ERROR_MESSAGE_POST);
}


/*
 * Print the invalid-CIGAR-character error message to STDERR.
 */
void
print_invalid_cigar_char_error(const char const *cigar_suffix_start,
                                const size_t cigar_suffix_len)
{
  /*
   * Print the individual parts of the invalid-CIGAR-character error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_cigar_error_message();
  fprintf(stderr, "%s", INVALID_CIGAR_CHAR_ERROR_MESSAGE_PRE);
  print_error_prefix(cigar_suffix_start, cigar_suffix_len);
  fprintf(stderr, "%s", INVALID_CIGAR_CHAR_ERROR_MESSAGE_POST);
}


/*
 * Print the missing-MD-match-count error message to STDERR.
 */
void
print_missing_md_match_count_error(const char const *md_suffix_start,
                                   const size_t md_suffix_len)
{
  /*
   * Print the individual parts of the missing-MD-match-count error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_md_tag_error_message();
  fprintf(stderr, "%s", MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_PRE);
  print_error_prefix(md_suffix_start, md_suffix_len);
  fprintf(stderr, "%s", MISSING_MD_MATCH_COUNT_ERROR_MESSAGE_POST);
}


/*
 * Print the invalid-MD-mismatch-nucleotide error message to STDERR.
 */
void
print_invalid_md_mismatch_nucleotide_error(const char const *md_suffix_start,
                                           const size_t md_suffix_len)
{
  /*
   * Print the individual parts of the invalid-MD-mismatch-nucleotide error
   * message to STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_md_tag_error_message();
  fprintf(stderr, "%s", INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_PRE);
  print_error_prefix(md_suffix_start, md_suffix_len);
  fprintf(stderr, "%s", INVALID_MD_MISMATCH_NUCLEOTIDE_ERROR_MESSAGE_POST);
}


/*
 * Print the missing-MD-deletion-nucleotide error message to STDERR.
 */
void
print_missing_md_deletion_nucleotide_error(const char const *md_suffix_start,
                                           const char const *md_suffix_end)
{
  /*
   * Print the individual parts of the missing-MD-deletion-nucleotide error
   * message to STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_md_tag_error_message();
  fprintf(stderr, "%s", MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE);
  print_error_prefix(md_suffix_start, md_suffix_end - md_suffix_start + 1);
  fprintf(stderr, "%s", MISSING_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST);
}


/*
 * Print the invalid-MD-deletion-nucleotide error message to STDERR.
 */
void
print_invalid_md_deletion_nucleotide_error(const char const *md_suffix_start,
                                           const size_t md_suffix_len)
{
  /*
   * Print the individual parts of the invalid-MD-deletion-nucleotide error
   * message to STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  print_invalid_md_tag_error_message();
  fprintf(stderr, "%s", INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_PRE);
  print_error_prefix(md_suffix_start, md_suffix_len);
  fprintf(stderr, "%s", INVALID_MD_DELETION_NUCLEOTIDE_ERROR_MESSAGE_POST);
}


/*
 * Print the incompatible-sequence-and-quality error message to STDERR.
 */
void
print_incompatible_sequence_and_quality_error(const char const *seq_start,
                                              const size_t seq_len,
                                              const char const *qual_start,
                                              const size_t qual_len,
                                              const char const *line)
{
  /*
   * Print the individual parts of the incompatible-sequence-and-quality error
   * message to STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  fprintf(stderr, "%s", INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_PRE);
  print_error_prefix(seq_start, seq_len);
  fprintf(stderr, "%s%lu%s",
          INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID1,
          (unsigned long) seq_len,
          INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID2);
  print_error_prefix(qual_start, qual_len);
  fprintf(stderr, "%s%lu%s%s%s",
          INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID3,
          (unsigned long) qual_len,
          INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_MID4,
          line,
          INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR_MESSAGE_POST);
}


/*
 * Print the incompatible-CIGAR-and-sequence error message to STDERR.
 */
void
print_incompatible_cigar_and_sequence_error()
{
  /*
   * Print the incompatible-CIGAR-and-sequence error message to STDERR.
   */
  fprintf(stderr, "%s", INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR_MESSAGE);
}


/*
 * Print the incompatible-CIGAR-and-MD error message to STDERR.
 */
void
print_incompatible_cigar_and_md_error()
{
  /*
   * Print the incompatible-CIGAR-and-MD error message to STDERR.
   */
  fprintf(stderr, "%s", INCOMPATIBLE_CIGAR_AND_MD_ERROR_MESSAGE);
}


/*
 * Print the invalid-nucleotide error message to STDERR.
 */
void
print_invalid_nucleotide_error(const char nucleotide)
{
  /*
   * Print the individual parts of the invalid-nucleotide error message to
   * STDERR.
   * Not actually concatenating them into a single string avoids the need to
   * allocate new memory to hold the full string that then would need to be
   * cleaned up again after printing.
   */
  fprintf(stderr, "%s%c%s",
          INVALID_NUCLEOTIDE_ERROR_MESSAGE_PRE, nucleotide,
          INVALID_NUCLEOTIDE_ERROR_MESSAGE_POST);
}
