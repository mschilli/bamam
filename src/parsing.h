/*****************************************************************************
 * BAMam parsing library header file                                         *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam parsing library header file.
 *
 * This is the header file for BAMam's parsing library.
 *
 * @file parsing.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-03-04
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_PARSING_H
#define BAMAM_PARSING_H


/************
 * includes *
 ************/

/*
 * Include standard definitions library to get the following type:
 * - `size_t` for unsigned integers large enough to hold any array index.
 */
#include <stddef.h>


/*************
 * functions *
 *************/

/**
 * @brief Update count.
 *
 * This function updates a given count based on a single character of the
 * corresponding string. If the given character is a digit, the count will be
 * updated (assuming the string is read in its entirety from left to right one
 * character at a time) and one is returned to indicate success. Otherwise, the
 * given count is left unmodified and zero is returned to indicate failure.
 *
 * @param count Location of count to update.
 * @param character Character to process.
 * @return One if @p character is a digit and @p count was updated accordingly,
 *         zero otherwise.
 *
 */
unsigned char
update_count(size_t *count, const char character);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_PARSING_H */
