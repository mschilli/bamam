/*****************************************************************************
 * BAMam MD tag parsing library header file                                  *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam MD tag parsing library header file.
 *
 * This is the header file for BAMam's MD tag parsing library.
 *
 * @file md.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-03-04 - 2019-04-09
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_MD_H
#define BAMAM_MD_H


/************
 * includes *
 ************/

/*
 * Include standard definitions library to get the following type:
 * - `size_t` for unsigned integers large enough to hold any array index.
 */
#include <stddef.h>


/*************
 * functions *
 *************/

/**
 * @brief Parse MD tag entry.
 *
 * This function parses a single MD tag entry (match count followed by single
 * missmatch nucleotide character or MD tag deletion character and one or more
 * deletion nucleotide characters) based on the beginning of the entry within
 * the MD tag and the (remaining) length of the MD tag (suffix) from that
 * position on.
 * If the given entry is valid, not only is the location of the (last)
 * nucleotide character of the MD tag entry returned, but also are the
 * corresponding match and deletion counts, and the remaining MD tag suffix
 * length written to the locations pointed at by @p match_count, @p del_count,
 * and @p len, respectively, as well as the no-error code to the
 * location pointed at by @p status to indicate success. If this is not the
 * case on the other hand, an error message is printed to STDERR, an error code
 * is written to the location pointed to by @p status to indicate the
 * invalidity of the MD tag entry, zero is written to the locations pointed to
 * by @p match_count, @p del_count, and @p len, and the null pointer is
 * returned.
 * See @f errors.h for the error code definition.
 *
 * @param cursor Location of the beginning (i.e. first digit) of the MD tag
 *               entry to parse.
 * @param len Location to read/write length of the MD tag (suffix) pointed to
 *            by @p cursor from/to.
 * @param match_count Location to write the parsed match count to.
 * @param del_count Location to write the parsed deletion count to.
 * @param status Location to write the (non-)error code to.
 * @return Location of the end (i.e. (last) nucleotide character) of the MD tag
 *         entry if valid, the null pointer otherwise.
 */
const char *
parse_md_entry(const char *cursor, size_t *const len,
               size_t *const match_count, size_t *const del_count,
               int *status);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_MD_H */
