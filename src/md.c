/*****************************************************************************
 * BAMam MD tag parsing library source file                                  *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam MD tag parsing library source file.
 *
 * This file contains the implementation of the BAMam MD tag parsing functions
 * defined in @f md.h.
 *
 * @file md.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-03-04 - 2019-04-10
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <md.h>

/*
 * Include BAMam parsing library to get the following function:
 * - `update_count` to update count.
 */
#include <parsing.h>

/*
 * Include BAMam nucleotide handling library to get the following function:
 * - `is_valid_nucleotide_char` to check if a character is a valid nucleotide
 *   character.
 */
#include <nucleotides.h>

/*
 * Include BAMam error code definition to get the following error codes:
 * - `NO_ERROR` to return upon success.
 * - `UNKNOWN_MD_TAG_PARSING_ERROR` to return when parsing an MD tag fails in an
 *   unexpected way (should never happen).
 * - `INVALID_MD_TAG_ERROR` to return when an MD tag is invalid.
 */
#include <errors.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify an invariant at runtime.
 */
#include <assert.h>

/*
 * Include BAMam message printing library to get the following functions:
 * - `print_missing_md_match_count_error` to print the missing-MD-match-count
 *   error message to STDERR.
 * - `print_invalid_md_mismatch_nucleotide_error` to print the
 *   invalid-MD-mismatch-nucleotide error message to STDERR.
 * - `print_missing_md_deletion_nucleotide_error` to print the
 *   missing-MD-deletion-nucleotide error message to STDERR.
 * - `print_invalid_md_deletion_nucleotide_error` to print the
 *   invalid-MD-deletion-nucleotide error message to STDERR.
 */
#include <messages.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Check if character is digit.
 *
 * This function checks for a single character, if it is a digit.
 *
 * @param character Character to check.
 * @return One if @p character is digit, zero otherwise.
 */
unsigned char
is_digit(const char character)
{

  /******************
   * initialization *
   *******************/

  /*
   * Define encoding if zero to improve code readability.
   */
  const unsigned char ZERO_CHAR = '0';

  /*
   * Define first non-single-digit-number to improve code readability.
   */
  const unsigned char TEN = 10;


  /*********
   * check *
   *********/

  /*
   * Convert the given character to its numeric value assuming it encodes a
   * digit and exit early reporting failure if it turns out not to.
   * Note: Regardless of the encoding used, all digit follow successively in
   * ascending order. Thus, for any character encoding a digit, its offset from
   * the character encoding the digit 'zero' equals its numeric values.
   * Characters encoded by higher values will end up with numeric values
   * greater or equal to ten. While characters encoded by lower values would
   * end up with negative numeric values, the unsigned nature of `digit` forces
   * them to underflow to positive values. As the numeric range that can be
   * represented by a (signed) `char` is big enough to hold the encodings of
   * all characters, so must an its unsigned counterpart. Therefore, this
   * conversion essentially implements a modulo-shift on a ring large enough to
   * hold all possible values, excluding the possibility of an overlap. Thus,
   * negative values won't only be mapped to positive values, but also to to
   * positive values larger than the largest positive value that can be reached
   * without integer underflow. This guarantees, that any non-digit character
   * will be assigned a unique numeric value larger or equal to ten.
   */
  const unsigned char digit = character - ZERO_CHAR;
  return (digit < TEN);
}


/**
 * @brief Check if character is MD tag deletion character.
 *
 * This function checks for a single character, if it is the MD tag deletion
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is the MD tag deletion character, zero
 *         otherwise.
 */
unsigned char
is_md_deletion_char(const char character)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define the MD tag deletion character to improve code readability.
   */
  const char MD_CHAR_DELETION = '^';


  /*********
   * check *
   *********/

  /*
   * Compare to the MD tag deletion character and report success upon a match.
   */
  return (character == MD_CHAR_DELETION);
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Parse MD tag entry.
 */
const char *
parse_md_entry(const char *cursor, size_t *const len,
               size_t *const match_count, size_t *const del_count,
               int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *status = UNKNOWN_MD_TAG_PARSING_ERROR;

  /*
   * Initialize the match count with zero (empty sum).
   */
  *match_count = 0;

  /*
   * Initialize the deletion count with zero (no deletion).
   */
  *del_count = 0;

  /*
   * Save the initial cursor position (for error output).
   */
  const char const *cursor_orig = cursor;


  /***************
   * match count *
   ***************/

  /*
   * Process the first digit and exit early reporting error if the given MD tag
   * entry begins with a non-digit character.
   * Note: The match and deletion counts were initialized to zero and should
   * not have changed when there was no digit. Instead of blindly relying on
   * this or setting them simply to improve code readability, assertions are
   * used to fail in a controlled manner if anything ever changes those
   * invariants.
   */
  if(!update_count(match_count, *cursor))
  {
    *status = INVALID_MD_TAG_ERROR;
    print_missing_md_match_count_error(cursor, *len);
    *len = 0;
    assert(*match_count == 0);
    assert(*del_count == 0);
    return NULL;
  }

  /*
   * This part of the code is only reached if the cursor points at a digit
   * which was already added to the match count.
   */

  /*
   * Move cursor to the second character of the MD tag and keep moving it
   * further down as long as there are digits left to process.
   */
  for(++cursor; --(*len) && update_count(match_count, *cursor); ++cursor);


  /**************
   * match only *
   **************/

  /*
   * This part of the code is only reached if at least one digit was processed
   * from the beginning of the MD tag entry, the match count was set to the
   * numeric value encoded by the processed digit(s), and either no non-digit
   * characters were found before reaching the end of the MD tag (suffix), or
   * the cursor points at the first non-digit character encountered.
   */

  /*
   * Exit early reporting success if the MD tag suffix consists of digits only.
   * Ensure the match counts is (re-)set to zero to indicate the invalidity of
   * the MD tag. Move the cursor back to the last digit to keep the invariant
   * of returning the end of the parsed MD tag entry as well as a non-zero
   * remaining length for valid MD tag entries.
   * Note: The deletion count was initialized to zero and should not have
   * changed when there was no character. Instead of blindly relying on this or
   * setting it simply to improve code readability, an assertion is used to
   * fail in a controlled manner if anything ever changes those invariants.
   */
  if(*len == 0)
  {
    ++(*len);
    --cursor;
    assert(*del_count == 0);
    *status = NO_ERROR;
    return cursor;
  }


  /************
   * mismatch *
   ************/

  /*
   * This part of the code is only reached if at least one digit was processed
   * from the beginning of the MD tag entry, the match count was set to the
   * numeric value encoded by the processed digit(s), and the cursor points at
   * the first non-digit character encountered.
   */

  /*
   * Exit early reporting error if the first (non-digit) character of the MD
   * tag (suffix) character of digits is neither the MD tag deletion character
   * nor a valid nucleotide character. Exit early reporting success, if it is a
   * valid nucleotide character. Ensure the match counts as well as the
   * remaining MD tag suffix length are (re-)set to zero to indicate the
   * invalidity of the MD tag.
   * Note: The deletion count was initialized to zero and should not have
   * changed when there was no character. Instead of blindly relying on this or
   * setting it simply to improve code readability, an assertion is used to
   * fail in a controlled manner if anything ever changes this invariant.
   */
  if(!is_md_deletion_char(*cursor))
  {
    assert(*del_count == 0);
    if(!is_valid_nucleotide_char(*cursor))
    {
      *status = INVALID_MD_TAG_ERROR;
      print_invalid_md_mismatch_nucleotide_error(cursor, *len);
      *len = 0;
      *match_count = 0;
      return NULL;
    }
    *status = NO_ERROR;
    return cursor;
  }


  /************
   * deletion *
   ************/

  /*
   * This part of the code is only reached if at least one digit was processed
   * from the beginning of the MD tag entry, followed by the MD tag deletion
   * character, the match count was set to the numeric value encoded by the
   * processed digit(s), and the cursor points at the (first) MD tag deletion
   * character following the match count.
   */

  /*
   * Move cursor to the first character following the MD tag deletion character
   * and keep moving it as long as there are valid nucleotide characters left.
   */
  --(*len);
  ++cursor;
  while(*len && is_valid_nucleotide_char(*cursor))
  {
    ++(*del_count);
    --(*len);
    ++cursor;
  }

  /*
   * Exit early reporting error if a character that is not a valid nucleotide
   * character is encountered before reaching the end of the MD tag (suffix) or
   * the next digit (i.e. the next MD tag entry). Ensure the match and deletion
   * counts as well as the remaining MD tag suffix length are (re-)set to zero
   * to indicate the invalidity of the MD tag.
   * Note: The deletion count was initialized to zero and should not have
   * changed when there was no character. Instead of blindly relying on this or
   * setting it simply to improve code readability, an assertion is used to
   * fail in a controlled manner if anything ever changes this invariant.
   */
  if(*len && !is_digit(*cursor))
  {
    *status = INVALID_MD_TAG_ERROR;
    print_invalid_md_deletion_nucleotide_error(cursor, *len);
    *len = 0;
    *match_count = 0;
    *del_count = 0;
    return NULL;
  }

  /*
   * Move the cursor back to the last valid character to keep the invariant
   * of returning the end of the parsed MD tag entry as well as a non-zero
   * remaining length for valid MD tag entries.
   */
  ++(*len);
  --cursor;

  /*
   * Exit early reporting error if the MD tag deletion character is not
   * followed by at least one valid nucleotide character. Ensure the match
   * count as well as the remaining MD tag suffix length are (re-)set to zero
   * to indicate the invalidity of the MD tag.
   */
  if(*del_count == 0)
  {
    *status = INVALID_MD_TAG_ERROR;
    print_missing_md_deletion_nucleotide_error(cursor_orig, cursor);
    *len = 0;
    *match_count = 0;
    return NULL;
  }


  /****************
   * finalization *
   ****************/

  /*
   * This part of the code is only reached if the MD tag entry consists of at
   * least one digit followed by the MD tag deletion character and at least one
   * valid nucleotide character, the match count was set to the numeric value
   * encoded by the digits, the deletion count was set to the number of valid
   * nucleotide characters following the MD tag deletion character, and the
   * cursor points to the (last) deletion nucleotide character.
   */

  /*
   * Pass up the location of the end (i.e. the (last) deletion nucleotide
   * character) of the parsed MD tag entry reporting success.
   */
  *status = NO_ERROR;
  return cursor;
}
