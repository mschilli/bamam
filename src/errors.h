/*****************************************************************************
 * BAMam error code definition header file                                   *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam error code definition header file.
 *
 * This header files defines BAMam's error codes.
 * As a general rule, positive error codes correspond to valid reasons to abort
 * BAMam and exit early while negative error codes correspond to unknown errors
 * that should never actually occur.
 *
 * @file errors.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-11 - 2019-04-12
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_ERRORS_H
#define BAMAM_ERRORS_H


/************
 * no error *
 ************/

/**
 * @brief No error code.
 *
 * This 'error code' indicates successful execution (i.e. no error occurred).
 */
static const int NO_ERROR = 0;


/*******************
 * expected errors *
 *******************/

/**
 * @brief Extra arguments error code.
 *
 * This error code indicates too many command line arguments.
 */
static const int TOO_MANY_ARGUMENTS_ERROR = 1;


/**
 * @brief Invalid command line flag error code.
 *
 * This error code indicates an invalid command line flag.
 */
static const int INVALID_FLAG_ERROR = 2;

/**
 * @brief Too few fields error code.
 *
 * This error code indicates an invalid input line with too few fields.
 */
static const int TOO_FEW_FIELDS_ERROR = 3;


/**
 * @brief Invalid SAM flag error code.
 *
 * This error code indicates an invalid SAM flag.
 */
static const int INVALID_SAM_FLAG_ERROR = 4;

/**
 * @brief Invalid CIGAR error code.
 *
 * This error code indicates an invalid input line with an invalid CIGAR
 * string.
 */
static const int INVALID_CIGAR_ERROR = 5;

/**
 * @brief Invalid MD tag error code.
 *
 * This error code indicates an invalid input line with an invalid MD tag.
 */
static const int INVALID_MD_TAG_ERROR = 6;

/**
 * @brief Incompatible CIGAR string and sequence error code.
 *
 * This error code indicates an invalid input line with an incompatible
 * combination of CIGAR string and sequence.
 */
static const int INCOMPATIBLE_CIGAR_AND_SEQUENCE_ERROR = 7;

/**
 * @brief Incompatible CIGAR string and MD tag error code.
 *
 * This error code indicates an invalid input line with an incompatible
 * combination of CIGAR string and MD tag.
 */
static const int INCOMPATIBLE_CIGAR_AND_MD_ERROR = 8;

/**
 * @brief Invalid nucleotide error code.
 *
 * This error code indicates an invalid input line with an invalid nucleotide.
 */
static const int INVALID_NUCLEOTIDE_ERROR = 9;

/**
 * @brief Incompatible sequence and quality string error code.
 *
 * This error code indicates an invalid input line with an incompatible
 * combination of sequence and quality string.
 */
static const int INCOMPATIBLE_SEQUENCE_AND_QUALITY_ERROR = 10;


/*********************
 * unexpected errors *
 *********************/

/**
 * @brief Unknown error code.
 *
 * This error code indicates an unknown error.
 * It should never be actually returned.
 */
static const int UNKNOWN_ERROR = -1;

/**
 * @brief Unknown argument parsing error code.
 *
 * This error code indicates an unknown error during argument parsing.
 * It should never be actually returned.
 */
static const int UNKNOWN_ARG_PARSING_ERROR = -2;

/**
 * @brief Unknown processing error code.
 *
 * This error code indicates an unknown error while processing an input line.
 * It should never be actually returned.
 */
static const int UNKNOWN_PROCESSING_ERROR = -3;

/**
 * @brief Unknown line parsing error code.
 *
 * This error code indicates an unknown error while parsing an input line.
 * It should never be actually returned.
 */
static const int UNKNOWN_LINE_PARSING_ERROR = -4;

/**
 * @brief Unknown annotation error code.
 *
 * This error code indicates an unknown error while annotating an input line.
 * It should never be actually returned.
 */
static const int UNKNOWN_ANNOTATION_ERROR = -5;

/**
 * @brief Unknown CIGAR parsing error code.
 *
 * This error code indicates an unknown error while parsing a CIGAR string.
 * It should never be actually returned.
 */
static const int UNKNOWN_CIGAR_PARSING_ERROR = -6;

/**
 * @brief Unknown MD tag parsing error code.
 *
 * This error code indicates an unknown error while parsing an MD tag.
 * It should never be actually returned.
 */
static const int UNKNOWN_MD_TAG_PARSING_ERROR = -7;

/**
 * @brief Unknown CIGAR processing error code.
 *
 * This error code indicates an unknown error while processing a CIGAR string.
 * It should never be actually returned.
 */
static const int UNKNOWN_CIGAR_PROCESSING_ERROR = -8;

/**
 * @brief Unknown sequence procressing error code.
 *
 * This error code indicates an unknown error while procressing a sequence.
 * It should never be actually returned.
 */
static const int UNKNOWN_SEQUENCE_PROCESSING_ERROR = -9;

/**
 * @brief Unknown MD tag procressing error code.
 *
 * This error code indicates an unknown error while procressing an MD tag.
 * It should never be actually returned.
 */
static const int UNKNOWN_MD_TAG_PROCESSING_ERROR = -10;


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_ERRORS_H */
