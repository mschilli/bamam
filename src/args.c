/*****************************************************************************
 * BAMam argument parsing library source file                                *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam argument parsing library source file.
 *
 * This file contains the implementation of the BAMam argument parsing
 * functions defined in @f args.h.
 *
 * @file args.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-11 - 2019-03-04
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <args.h>

/*
 * Include standard string library to get the following function:
 * - `strcmp` to compare two (null-terminated) strings.
 */
#include <string.h>

/*
 * Include BAMam error code definition to get the following error codes:
 * - `UNKNOWN_ARG_PARSING_ERROR` to return when the argument parsing fails in
 *   an unexpected way (should never happen).
 * - `NO_ERROR` to return upon success.
 * - `TOO_MANY_ARGUMENTS_ERROR` to return when too many command line arguments
 *   are passed to BAMam.
 * - `INVALID_FLAG_ERROR` to return when the and invalid command line flag is
 *   passed to BAMam.
 */
#include <errors.h>

/*
 * Include standard assertion header to get the following macro:
 * - `assert` to verify an invariant at runtime.
 */
#include <assert.h>

/*
 * Include BAMam message printing library to get the following function:
 * - `print_too_many_arguments_error` to print the too-many-arguments usage
 *   error message to STDERR.
 * - `print_help` to print the help message to STDOUT.
 * - `print_version` to print the version to STDOUT.
 * - `print_invalid_flag_error` to print the invalid-flag usage error message
 *   to STDERR.
 */
#include <messages.h>


/********************
 * helper functions *
 ********************/

/**
 * @brief Compare argument against valid flags.

 * This function checks if the given argument matches a given valid flag.

 * @param arg Command line argument to compare to valid help flags.
 * @param short_flag Valid short option flag (e.g. `"-h"`) to check for.
 * @param long_flag Valid long option flag (e.g. `"--help"`) to check for.
 * @return `1` if @p arg equals @p short_flag or @p long_flag, `0` otherwise.
 */
unsigned char
check_flag(const char const *arg,
           const char const *short_flag,
           const char const *long_flag)
{
  /**************
   * short flag *
   **************/

  /*
   * Exit early and report a match if the given argument matches the valid
   * short option flag.
   */
  if(!strcmp(arg, short_flag))
  {
    return 1;
  }


  /*************
   * long flag *
   *************/

  /*
   * Due to the above `return` statement, this part of the function will only
   * execute if the given argument did not match the valid short option flag.
   * Thus, the only way for it to match the given valid flag would be for it to
   * match the corresponding long option flag.
   */
  return !strcmp(arg, long_flag);
}


/**
 * @brief Test for help flag.

 * This function checks if a given command line argument is a valid help flag
 * (`-h` or `--help`).

 * @param arg command line argument to compare to valid help flags.
 * @return `1` if @p arg is a valid help flag, `0` otherwise.
 */
unsigned char
is_flag_help(const char const *arg)
{
  /*
   * Determine if a given argument is a valid help flag by checking for the
   * corresponding short and long option command line flags.
   */
  return check_flag(arg, "-h", "--help");
}


/**
 * @brief Test for version flag.

 * This function checks if a given command line argument is a valid version
 * flag (`-V` or `--version`).
 * Note: The `-v` flag remains reserved to keep backwards compatibility in case
 * a verbosity flag is introduced later on.

 * @param arg command line argument to compare to valid version flags.
 * @return `1` if @p arg is a valid version flag, `0` otherwise.
 */
unsigned char
is_flag_version(const char const *arg)
{
  /*
   * Determine if a given argument is a valid version flag by checking for the
   * corresponding short and long option command line flags.
   */
  return check_flag(arg, "-V", "--version");
}


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/*
 * Verify invariants and parse arguments.
 * BAMam only supports up to a single argument.
 * This argument must be a valid help flag.
 */
unsigned char
parse_args(int argc, const char *const *argv, int *error)
{

  /******************
   * initialization *
   ******************/

  /*
   * Initialize the error status with a special code to catch any potentially
   * unhandled error.
   */
  *error = UNKNOWN_ARG_PARSING_ERROR;

  /*
   * Define return values to improve code readability.
   */
  const unsigned char CONTINUE = 1;
  const unsigned char STOP = 0;

  /*
   * Define expected argument counts to improve code readability.
   * Note that this includes `argv[0]`, the command to run BAMam itself.
   */
  const unsigned char ARGC_NO_ARGS = 1;  /* no extra arguments */
  const unsigned char ARGC_MAX_ARGS = 2; /* a single extra argument */

  /*
   * If this function is called the way it is supposed to be, `argc` should
   * equal the argument count at the beginning of the main function, i.e. the
   * length of the `argv` array which must contain at least the command used to
   * invoke BAMam itself.
   * While the former invariant cannot be verified easily, at least the second
   * one can be checked under the assumption of the first as it rules out
   * negative values and zero for `argc`.
   */
  assert(argc >= 1);


  /****************
   * no arguments *
   ****************/

  /*
   * Exit early reporting success (and encourage the main program to move on)
   * if no extra arguments were passed down.
   */
  if(argc == ARGC_NO_ARGS)
  {
    *error = NO_ERROR;
    return CONTINUE;
  }


  /**********************
   * too many arguments *
   **********************/

  /*
   * Report usage error if too many arguments were passed down.
   */
  if(argc > ARGC_MAX_ARGS)
  {
    print_too_many_arguments_error(argv[0], argc, ARGC_MAX_ARGS);
    *error = TOO_MANY_ARGUMENTS_ERROR;
    return STOP;
  }


  /*************
   * help flag *
   *************/

  /*
   * This part of the code should only be reached if exactly one command line
   * argument was passed down.
   */
  assert(argc == 2);

  /*
   * Print the help message and exit early reporting success (but discourage
   * the main program to move on) if the only command line argument is a valid
   * help flag.
   */
  if(is_flag_help(argv[1]))
  {
    print_help(argv[0]);
    *error = NO_ERROR;
    return STOP;
  }


  /****************
   * version flag *
   ****************/

  /*
   * Print the version and exit early reporting success (but discourage the
   * main program to move on) if the only command line argument is a valid
   * version flag.
   */
  if(is_flag_version(argv[1]))
  {
    print_version();
    *error = NO_ERROR;
    return STOP;
  }


  /****************
   * invalid flag *
   ****************/

  /*
   * Report usage error for any invalid command line flag.
   */
  print_invalid_flag_error(argv[0], argv[1]);
  *error = INVALID_FLAG_ERROR;
  return STOP;
}
