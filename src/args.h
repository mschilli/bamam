/*****************************************************************************
 * BAMam argument parsing library header file                                *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam argument parsing library header file.
 *
 * This is the header file for BAMam's argument parsing library.
 * The functionality provided here could be achieved using the `getopt` library
 * but given that BAMam is not supposed to take any command line arguments but
 * the standard help and version flags, even a dependency this small and common
 * seems unnecessary.
 *
 * @file args.h
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-01-11 - 2019-03-04
 */


/*************************
 * include guard (begin) *
 *************************/

/*
 * Ensure this header file is not included more than once using the
 * non-standard but widely supported compiler pragma introduced for that
 * purpose.
 */
#pragma once

/*
 * In case BAMam is compiled with a compiler not supporting `#pragma once`
 * or the same header is included from different paths, fall back to the common
 * practice of testing for and defining a so-called include guard pre-processor
 * macro.
 * To minimize the probability of that guard clashing with a 3rd-party header,
 * the project name is included in the header guard name.
 */
#ifndef BAMAM_ARGS_H
#define BAMAM_ARGS_H


/*************
 * functions *
 *************/

/**
 * @brief Check command line arguments.
 *
 * This function checks if the given command line arguments are valid.
 * If an error occurs or a message is queried by the given flags, the
 * corresponding (error) message is issued.
 * If this function returns `0` (false), the main program should not continue
 * but instead pass up the return value stored at the location given by the
 * @p error pointer.
 * See @f errors.h for the error code definition.
 *
 * @param argc Command line argument count.
 * @param argv Command line arguments.
 * @param error Location to store store error code at.
 * @return `1` if the main program should continue with the given parameters,
 * `0` otherwise.
 */
unsigned char
parse_args(const int argc, const char *const *argv, int *error);


/***********************
 * include guard (end) *
 ***********************/

#endif /* BAMAM_ARGS_H */
