/*****************************************************************************
 * BAMam nucleotide handling library source file                             *
 *                                                                           *
 * Copyright (C) 2019  Marcel Schilling <marcel.schilling@mdc-berlin.de>     *
 *                                                                           *
 * This file is part of BAMam.                                               *
 *                                                                           *
 * BAMam is free software: you can redistribute it and/or modify             *
 * it under the terms of the GNU Affero General Public License as            *
 * published by the Free Software Foundation, either version 3 of the        *
 * License, or (at your option) any later version.                           *
 *                                                                           *
 * This program is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU Affero General Public License for more details.                       *
 *                                                                           *
 * You should have received a copy of the GNU Affero General Public License  *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *****************************************************************************/


/***********************
 * general information *
 ***********************/

/**
 * @brief BAMam nucleotide handling library source file.
 *
 * This file contains the implementation of the BAMam nucleotide handling
 * functions defined in @f nucleotides.h.
 *
 * @file nucleotides.c
 * @author [Marcel Schilling](mailto:marcel.schilling@mdc-berlin.de)
 * @date 2019-04-10 - 2019-04-15
 */


/************
 * includes *
 ************/

/*
 * Include the header file corresponding to the library implemented here.
 */
#include <nucleotides.h>


/*
 * Include BAMam error code definition to get the following error codes:
 * - `NO_ERROR` to return upon success.
 * - `INVALID_NUCLEOTIDE_ERROR` to return when a nucleotide character is
 *   invalid.
 */
#include <errors.h>

/*
 * Include BAMam message printing library to get the following function:
 * - `print_invalid_nucleotide_error` to print the invalid-nucleotide error
 *   message to STDERR.
 */
#include <messages.h>


/********************
 * public functions *
 ********************/

/*
 * See the header file for public function interface documentation.
 * Using a library should not require reading anything but the header file.
 * Duplicating the API documentation here could result in documentation
 * going out of sync.
 *
 * This is a concious decision to increase user convenience at the cost of
 * developer convenience. Given that this decision had to be made by a
 * developer, this is the only fair (as in non-egoistic) choice to make.
 * The side effect that changing documentation comments forces rebuilds of all
 * client code is considered a feature in this context: When it comes to APIs,
 * documentation (and thereby comments) should be considered code.
 *
 * That said, comments not concerning the interface documentation belong in the
 * source file and the source file only.
 */


/**
 * @brief Check if character is valid nucleotide character.
 *
 * This function checks for a single character, if it is a valid nucleotide
 * character.
 *
 * @param character Character to check.
 * @return One if @p character is a valid nucleotide character, zero otherwise.
 */
unsigned char
is_valid_nucleotide_char(const char character)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define all valid nucleotide characters to improve code readability.
   * Source: https://en.wikipedia.org/wiki/Nucleic_acid_notation#IUPAC_notation
   */
  const char NUCLEOTIDE_CHAR_ADENINE = 'A';
  const char NUCLEOTIDE_CHAR_CYTOSINE = 'C';
  const char NUCLEOTIDE_CHAR_GUANINE = 'G';
  const char NUCLEOTIDE_CHAR_THYMINE = 'T';
  const char NUCLEOTIDE_CHAR_WEAK = 'W';
  const char NUCLEOTIDE_CHAR_STRONG = 'S';
  const char NUCLEOTIDE_CHAR_AMINO = 'M';
  const char NUCLEOTIDE_CHAR_KETO = 'K';
  const char NUCLEOTIDE_CHAR_PURINE = 'R';
  const char NUCLEOTIDE_CHAR_PYRIMIDINE = 'Y';
  const char NUCLEOTIDE_CHAR_NOT_ADENINE = 'B';
  const char NUCLEOTIDE_CHAR_NOT_CYTOSINE = 'D';
  const char NUCLEOTIDE_CHAR_NOT_GUANINE = 'H';
  const char NUCLEOTIDE_CHAR_NOT_THYMINE = 'V';
  const char NUCLEOTIDE_CHAR_ANY = 'N';


  /*********
   * check *
   *********/

  /*
   * Compare to all valid nucleotide characters and report success on
   * the first match.
   * Note: Due to C's short-circuit evaluation, performance of this statement
   * improves if the comparisons are ordered from most to least common. This is
   * especially true since the no-match case can only be reached once on
   * invalid input before erroring out.
   */
  return (   character == NUCLEOTIDE_CHAR_ADENINE
          || character == NUCLEOTIDE_CHAR_THYMINE
          || character == NUCLEOTIDE_CHAR_GUANINE
          || character == NUCLEOTIDE_CHAR_CYTOSINE
          || character == NUCLEOTIDE_CHAR_WEAK
          || character == NUCLEOTIDE_CHAR_STRONG
          || character == NUCLEOTIDE_CHAR_AMINO
          || character == NUCLEOTIDE_CHAR_KETO
          || character == NUCLEOTIDE_CHAR_PURINE
          || character == NUCLEOTIDE_CHAR_PYRIMIDINE
          || character == NUCLEOTIDE_CHAR_NOT_ADENINE
          || character == NUCLEOTIDE_CHAR_NOT_CYTOSINE
          || character == NUCLEOTIDE_CHAR_NOT_GUANINE
          || character == NUCLEOTIDE_CHAR_NOT_THYMINE
          || character == NUCLEOTIDE_CHAR_ANY);
}


/**
 * @brief Complement nucleotide character.
 *
 * This function returns the complement of a given nucleotide character.
 * If the given character is a valid nucleotide, not only its complement
 * returned, but also is the no-error code to the location pointed at by
 * @p status to indicate success. If this is not the case on the other hand, an
 * error message is printed to STDERR, an error code is written to the location
 * pointed to by @p status to indicate the invalidity of the nucleotide, and
 * the null character is returned.
 * See @f errors.h for the error code definition.
 *
 * @param nucleotide Nucleotide character to return the complement for.
 * @param status Location to write the (non-)error code to.
 * @return The complement nucleotide character of @p nucleotide, if it is a
 *         valid nucleotide character, the null character otherwise.
 */
char
complement(const char nucleotide, int *status)
{

  /******************
   * initialization *
   ******************/

  /*
   * Define all valid nucleotide characters to improve code readability.
   * Source: https://en.wikipedia.org/wiki/Nucleic_acid_notation#IUPAC_notation
   */
  const char NUCLEOTIDE_CHAR_ADENINE = 'A';
  const char NUCLEOTIDE_CHAR_CYTOSINE = 'C';
  const char NUCLEOTIDE_CHAR_GUANINE = 'G';
  const char NUCLEOTIDE_CHAR_THYMINE = 'T';
  const char NUCLEOTIDE_CHAR_WEAK = 'W';
  const char NUCLEOTIDE_CHAR_STRONG = 'S';
  const char NUCLEOTIDE_CHAR_AMINO = 'M';
  const char NUCLEOTIDE_CHAR_KETO = 'K';
  const char NUCLEOTIDE_CHAR_PURINE = 'R';
  const char NUCLEOTIDE_CHAR_PYRIMIDINE = 'Y';
  const char NUCLEOTIDE_CHAR_NOT_ADENINE = 'B';
  const char NUCLEOTIDE_CHAR_NOT_CYTOSINE = 'D';
  const char NUCLEOTIDE_CHAR_NOT_GUANINE = 'H';
  const char NUCLEOTIDE_CHAR_NOT_THYMINE = 'V';
  const char NUCLEOTIDE_CHAR_ANY = 'N';

  /*
   * Define error character to improve code readability.
   */
  const char ERROR_CHAR = '\0';


  /*********
   * valid *
   *********/

  /*
   * Compare the given character against all valid nucleotide characters and
   * return the corresponding complement nucleotide character reporting success
   * on the first match.
   * Note: The readable constants can not be used as case labels, as a switch
   * statement requires compile time constants. Those could be achieved by
   * using macros (via `#define` instead) but this would declare globals which
   * also seems suboptimal. Unsetting them again after the switch just seemed
   * too much of boilerplate and the literal character constants where deemed
   * 'readable enough'.
   * Note: Due to the return statements, no break statement as required to
   * isolate each case from the following one(s).
   * Note: Performance of this statement improves if the cases are ordered from
   * most to least common. This is especially true since the no-match case can
   * only be reached once on invalid input before erroring out.
   */
  switch(nucleotide)
  {
    case 'A':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_THYMINE;
    case 'T':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_ADENINE;
    case 'G':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_CYTOSINE;
    case 'C':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_GUANINE;
    case  'W':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_WEAK;
    case  'S':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_STRONG;
    case  'M':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_KETO;
    case  'K':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_AMINO;
    case  'R':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_PYRIMIDINE;
    case  'Y':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_PURINE;
    case  'B':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_NOT_THYMINE;
    case  'D':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_NOT_GUANINE;
    case  'H':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_NOT_CYTOSINE;
    case  'V':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_NOT_ADENINE;
    case 'N':
      *status = NO_ERROR;
      return NUCLEOTIDE_CHAR_ANY;
  }


  /***********
   * invalid *
   ***********/

  /*
   * This part of the code is only reached, if the given character does not
   * match any valid nucleotide character.
   */

  /*
   * Print error message and report failure.
   */
  *status = INVALID_NUCLEOTIDE_ERROR;
  print_invalid_nucleotide_error(nucleotide);
  return ERROR_CHAR;
}
