# BAMam version 0.2.2 alpha

## Bugfixes

* Added support for unmapped reads.



# BAMam version 0.2.1 alpha

## Bugfixes

* Added support for non-ACGTN reference nucleotides.



# BAMam version 0.2.0 alpha

## New features (breaking changes)

* Added quality scores to tag entries.



# BAMam version 0.1.1 alpha

## Bugfixes

* Added support for uncalled bases in the read.



# BAMam version 0.1 alpha

* First fully functional version of BAMam (to the extend covered by the example
alignments in the test data).
